<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Provider;

/**
 * Interface IProviderCall
 * @package Snooper\Components\Provider
 */
interface IProviderCall
{
    /**
     * @param IProvider $provider
     * @param $method
     * @param null $additionalParameters
     * @return mixed
     */
    public static function create(IProvider $provider, $method, $additionalParameters = null);

    /**
     * @return IProvider
     */
    public function getProvider();

    /**
     * @return string
     */
    public function getMethod();

    /**
     * @return mixed
     */
    public function getAdditionalParameters();
}
