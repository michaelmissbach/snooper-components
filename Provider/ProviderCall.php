<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Provider;

/**
 * Class ProviderCall
 * @package Snooper\Components\Provider
 * @internal
 */
class ProviderCall implements IProviderCall
{
    /**
     * @var IProvider
     */
    protected $provider;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var mixed
     */
    protected $additionalParameters;

    /**
     * EventCall constructor.
     * @param IProvider $provider
     * @param $method
     * @param null $additionalParameters
     */
    public function __construct(IProvider $provider, $method, $additionalParameters = null)
    {
        $this->provider             = $provider;
        $this->method               = $method;
        $this->additionalParameters = $additionalParameters;
    }

    /**
     * @param IProvider $provider
     * @param $method
     * @param null $additionalParameters
     * @return ProviderCall
     */
    public static function create(IProvider $provider, $method, $additionalParameters = null)
    {
        return new static($provider, $method, $additionalParameters);
    }

    /**
     * @return IProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return mixed
     */
    public function getAdditionalParameters()
    {
        return $this->additionalParameters;
    }
}
