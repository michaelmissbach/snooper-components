<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Provider;

/**
 * Interface IProviderCallDefinition
 * @package Snooper\Components\Provider
 */
interface IProviderCallDefinition
{
    const PRIORIZATION_LOWEST = -255;

    const PRIORIZATION_STANDARD = 0;

    const PRIORIZATION_HIGHEST = 255;

    /**
     * @param $method
     * @param null $additionalParameters
     * @param int $priorization
     * @return mixed
     */
    public static function create($method, $additionalParameters = null, $priorization = self::PRIORIZATION_STANDARD);

    /**
     * @return string
     */
    public function getMethod();

    /**
     * @return int
     */
    public function getPriorization();

    /**
     * @return mixed
     */
    public function getAdditionalParameters();

    /**
     * @param string $method
     * @return IProviderCallDefinition
     */
    public function setMethod($method);

    /**
     * @param int $priorization
     * @return IProviderCallDefinition
     */
    public function setPriorization($priorization);

    /**
     * @param mixed $additionalParameters
     * @return IProviderCallDefinition
     */
    public function setAdditionalParameters($additionalParameters);
}
