<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Provider;

/**
 * Class ProviderCallDefinition
 * @package Snooper\Components\Provider
 */
class ProviderCallDefinition implements IProviderCallDefinition
{
    /**
     * @var string
     */
    protected $method;

    /**
     * @var int
     */
    protected $priorization;

    /**
     * @var mixed
     */
    protected $additionalParameters;

    /**
     * EventDefinition constructor.
     * @param $method
     * @param $additionalParameters
     * @param $priorization
     */
    public function __construct($method, $additionalParameters = null, $priorization = self::PRIORIZATION_STANDARD)
    {
        $this->method               = $method;
        $this->additionalParameters = $additionalParameters;
        $this->priorization         = $priorization;
    }

    /**
     * @param $method
     * @param null $additionalParameters
     * @param int $priorization
     * @return ProviderCallDefinition
     */
    public static function create($method, $additionalParameters = null, $priorization = self::PRIORIZATION_STANDARD)
    {
        return new static($method, $additionalParameters, $priorization);
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return int
     */
    public function getPriorization()
    {
        return $this->priorization;
    }

    /**
     * @return mixed
     */
    public function getAdditionalParameters()
    {
        return $this->additionalParameters;
    }

    /**
     * @param string $method
     * @return ProviderCallDefinition
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param int $priorization
     * @return ProviderCallDefinition
     */
    public function setPriorization($priorization)
    {
        $this->priorization = $priorization;
        return $this;
    }

    /**
     * @param mixed $additionalParameters
     * @return ProviderCallDefinition
     */
    public function setAdditionalParameters($additionalParameters)
    {
        $this->additionalParameters = $additionalParameters;
        return $this;
    }
}
