<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Provider;

use Snooper\Components\Utility\Traits\Container\Container;
use Snooper\Components\Utility\Traits\Container\ContainerGrouped;
use Snooper\Components\Utility\Traits\Container\Sortable;
use Snooper\Components\Utility\Traits\Container\SortFlat;
use Snooper\Components\Utility\Traits\Container\SortGroup;
use Snooper\Components\Utility\Traits\Singleton;

/**
 * Class ProviderCallContainer
 * @package Snooper\Components\Provider
 */
class ProviderCallContainer
{
    use Singleton;
    use Container;
    use ContainerGrouped;
    use Sortable;
    use SortFlat;
    use SortGroup;

    /**
     * @param IProviderCall $eventCall
     * @param int $prio
     */
    public function add(IProviderCall $eventCall, $prio = 0, $forcePrio = false)
    {
        $prio = (int)$prio;

        if (!$forcePrio) {
            if ($prio < IProviderCallDefinition::PRIORIZATION_LOWEST) {
                $prio = IProviderCallDefinition::PRIORIZATION_LOWEST;
            }
            if ($prio > IProviderCallDefinition::PRIORIZATION_HIGHEST) {
                $prio = IProviderCallDefinition::PRIORIZATION_HIGHEST;
            }
        }

        if (!is_array($this->container)) {
            $this->container = [];
        }
        if (!array_key_exists($prio,$this->container)) {
            $this->container[$prio] = [];
        }
        $this->container[$prio][] = $eventCall;
        $this->count++;
    }

    /**
     * @return array|\Generator
     */
    public function &getDeepYielded()
    {
        foreach ($this->container as $group) {
            foreach ($group as $entry) {
                yield $entry;
            }
        }
    }
}
