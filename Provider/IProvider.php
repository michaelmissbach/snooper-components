<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Provider;

use Snooper\Components\EventTrigger\IEventTrigger;

/**
 * Interface IProvider
 * @package Snooper\Components\Provider
 */
interface IProvider
{
    /**
     * @return bool
     */
    public function isActive();

    /**
     * @return array
     */
    public function getEvents();

    /**
     * @param IEventTrigger $e
     * @return mixed
     */
    public function preRaiseEvent(IEventTrigger $e);

    /**
     * @param IEventTrigger $e
     * @return mixed
     */
    public function postRaiseEvent(IEventTrigger $e);
}
