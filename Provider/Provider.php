<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Provider;

use Snooper\Components\EventTrigger\IEventTrigger;

/**
 * Class Provider
 * @package Snooper\Components\Provider
 */
abstract class Provider implements IProvider
{
    /**
     * @return bool
     */
    public abstract function isActive();

    /**
     * @return array
     */
    public abstract function getEvents();

    /**
     * @param IEventTrigger $e
     * @return mixed
     */
    public function preRaiseEvent(IEventTrigger $e)
    {
        return null;
    }

    /**
     * @param IEventTrigger $e
     * @return mixed
     */
    public function postRaiseEvent(IEventTrigger $e)
    {
        return null;
    }
}
