<?php return [
    \Snooper\Components\Response\Interaction\Response::class,
    \Snooper\Components\Response\Variable\Response::class,
    \Snooper\Components\Response\Standard\Response::class,
    \Snooper\Components\Response\Raw\Response::class
];