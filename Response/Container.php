<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response;

use Snooper\Components\Utility\Traits\Container\ContainerFlat;
use Snooper\Components\Utility\Traits\Singleton;

/**
 * Class Container
 * @package Snooper\Components\Response\Standard
 */
class Container
{
    use Singleton;
    use \Snooper\Components\Utility\Traits\Container\Container;
    use ContainerFlat;
}
