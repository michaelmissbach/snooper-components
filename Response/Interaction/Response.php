<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Interaction;

use Snooper\Components\Provider\IProvider;
use Snooper\Components\Response\ICommonResponse;
use Snooper\Components\Response\Interaction\Options\IInteractionOption;
use Snooper\Components\Response\Parameters\Deliver;
use Snooper\Components\Response\Parameters\IDeliver;
use Snooper\Components\Response\Parameters\Parameter;
use Snooper\Components\Snooper;

/**
 * Class Response
 * @package Snooper\Components\Response\Interaction
 */
class Response implements ICommonResponse, IResponse
{
    /**
     * @var string
     */
    protected $source;

    /**
     * @var Interaction
     */
    protected $interaction;

    /**
     * @var string
     */
    protected $javascriptMethodName;

    /**
     * @var mixed
     */
    protected $javascriptParameter;

    /**
     * @var boolean
     */
    protected $delivered = false;

    /**
     * @var []
     */
    protected $options;

    /**
     * @var IDeliver
     */
    protected $deliver;

    /**
     * Response constructor.
     * @param IInteraction $interaction
     * @param $javascriptMethodName
     * @param null $javascriptParameter
     * @param mixed $options
     * @throws \Exception
     */
    protected function __construct(IInteraction $interaction, $javascriptMethodName, $javascriptParameter = null, $options = null)
    {
        $this->interaction = $interaction;

        $this->javascriptMethodName = $javascriptMethodName;

        if ($javascriptParameter !== null) {
            if ($javascriptParameter instanceof Parameter) {
                $this->javascriptParameter = $javascriptParameter;
            } else {
                //todo: check where and if the error raises and if its indicates on frontend
                $this->javascriptMethodName = "Snooper.log";
                $this->javascriptParameter = Parameter::createFromList(
                    sprintf(
                        'Snooper error: The parameter "javascriptParameter" on method call "%s" has to be an instance of Snooper\Components\Response\Parameters\IParameter.',
                        $javascriptMethodName
                    )
                    ,false
                );
            }
        }

        if ($options !== null ) {
            foreach (is_array($options) ? $options : [$options] as $option) {
                if (!($option instanceof IInteractionOption)) {
                    continue;
                }
                if (!is_array($this->options)) {
                    $this->options = [];
                }
                $this->options[] = $option;
            }
        }

        $this->deliver = Deliver::create(Snooper::instance()->getConfig()->getLastSection(),Deliver::LAST_SECTION);
    }

    public static function create(IInteraction $interaction, $javascriptMethodName, $javascriptParameter = null, $options = null)
    {
        return new static($interaction,$javascriptMethodName,$javascriptParameter,$options);
    }

    /**
     * @param IProvider $provider
     */
    public function setProvider(IProvider $provider)
    {
        $this->source = get_class($provider);
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return Interaction
     */
    public function getInteraction()
    {
        return $this->interaction;
    }

    /**
     * @return string
     */
    public function getJavascriptMethodName()
    {
        return $this->javascriptMethodName;
    }

    /**
     * @return mixed
     */
    public function getJavascriptParameter()
    {
        return $this->javascriptParameter;
    }

    /**
     * @return []
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return bool
     */
    public function isDelivered()
    {
        return $this->delivered;
    }

    /**
     * @param bool $delivered
     * @return Response
     */
    public function setDelivered($delivered)
    {
        $this->delivered = $delivered;
        return $this;
    }

    /**
     * @return IDeliver
     */
    public function getDeliver()
    {
        return $this->deliver;
    }

    /**
     * @return mixed
     */
    public function prepare()
    {
        $return = [];

        $return['type'] = 'interaction';

        $return['source'] = $this->getSource();
        $return['target'] = $this->getInteraction()->getTarget();
        $return['event']  = $this->getInteraction()->getEvent();

        if (is_array($this->options)) {
            /** @var IInteractionOption $option */
            foreach($this->options as $option) {
                if (!($option instanceof IInteractionOption)) {
                    continue;
                }
                $return[$option->getJsConfigKey()] = $option->getConfig();
            }
        }
        $return['method'] = $this->getJavascriptMethodName();

        if ($this->getJavascriptParameter() instanceof Parameter) {
            $return['params'] = $this->getJavascriptParameter()->getArguments();
        } else {
            $return['params'] = null;
        }

        return $return;
    }

    /**
     * @return array|string
     */
    public function getSendResponseLog()
    {
        return sprintf('Add interaction response from "%s" ("%s"-"%s").',$this->getSource(),$this->getInteraction()->getTarget(),$this->getInteraction()->getEvent());
    }
}
