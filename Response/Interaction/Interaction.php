<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Interaction;

use Snooper\Components\Exception\NotAllowedValueException;
use Snooper\Components\Utility\Traits\ConstantFinderTrait;

/**
 * Class Interaction
 * @package Snooper\Components\Response\Interaction
 */
class Interaction implements IInteraction
{
    use ConstantFinderTrait;

    /**
     * @var array
     */
    protected $__constants = [
        'ON_AFTERPRINT'=>'afterprint',
        'ON_BERFOREPRINT'=>'beforeprint',
        'ON_BEFOREUNLOAD'=>'beforeunload',
        'ON_HASHCHANGE'=>'hashchange',
        'ON_LOAD'=>'load',
        'ON_MESSAGE'=>'message',
        'ON_OFFLINE'=>'offline',
        'ON_ON_LINE'=>'online',
        'ON_PAGEHIDE'=>'pagehide',
        'ON_PAGESHOW'=>'pageshow',
        'ON_POPSTATE'=>'popstate',
        'ON_RESIZE'=>'resize',
        'ON_STORAGE'=>'storage',
        'ON_UNLOAD'=>'unload',
        'ON_BLUR'=>'blur',
        'ON_CHANGE'=>'change',
        'ON_CON_TEXTMENU'=>'contextmenu',
        'ON_FOCUS'=>'focus',
        'ON_INPUT'=>'input',
        'ON_INVALID'=>'invalid',
        'ON_RESET'=>'reset',
        'ON_SEARCH'=>'search',
        'ON_SELECT'=>'select',
        'ON_SUBMIT'=>'submit',
        'ON_KEYDOWN'=>'keydown',
        'ON_KEYPRESS'=>'keypress',
        'ON_KEYUP'=>'keyup',
        'ON_CLICK'=>'click',
        'ON_DBLCLICK'=>'dblclick',
        'ON_MOUSEDOWN'=>'mousedown',
        'ON_MOUSEMOVE'=>'mousemove',
        'ON_MOUSEOUT'=>'mouseout',
        'ON_MOUSEOVER'=>'mouseover',
        'ON_MOUSEUP'=>'mouseup',
        'ON_MOUSEWHEEL'=>'mousewheel',
        'ON_WHEEL'=>'wheel',
        'ON_DRAG'=>'drag',
        'ON_DRAGEND'=>'dragend',
        'ON_DRAGCENTER'=>'dragenter',
        'ON_DRAGLEAVE'=>'dragleave',
        'ON_DRAGOVER'=>'dragover',
        'ON_DRAGSTART'=>'dragstart',
        'ON_DROP'=>'drop',
        'ON_SCROLL'=>'scroll',
        'ON_COPY'=>'copy',
        'ON_CUT'=>'cut',
        'ON_PASTE'=>'paste',
        'ON_ABORT'=>'abort',
        'ON_CANPLAY'=>'canplay',
        'ON_CANPLAYTHROUGH'=>'canplaythrough',
        'ON_CUECHANGE'=>'cuechange',
        'ON_DURATION_CHANGE'=>'durationchange',
        'ON_EMPTIED'=>'emptied',
        'ON_ENDED'=>'ended',
        'ON_ERROR'=>'error',
        'ON_LOADEDDATA'=>'loadeddata',
        'ON_LOADEDMETADATA'=>'loadedmetadata',
        'ON_LOADSTART'=>'loadstart',
        'ON_PAUSE'=>'pause',
        'ON_PLAY'=>'play',
        'ON_PLAYING'=>'playing',
        'ON_PROGRESS'=>'progress',
        'ON_RATECHANGE'=>'ratechange',
        'ON_SEEKED'=>'seeked',
        'ON_SEEKING'=>'seeking',
        'ON_STALLED'=>'stalled',
        'ON_SUSPEND'=>'suspend',
        'ON_TIMEUPDATE'=>'timeupdate',
        'ON_VOLUMECHANGE'=>'volumechange',
        'ON_WAITING'=>'waiting',
        'ON_TOGGLECON_ST'=>'toggle'
    ];

    const ON_AFTERPRINT='afterprint';
    const ON_BERFOREPRINT='beforeprint';
    const ON_BEFOREUNLOAD='beforeunload';
    const ON_HASHCHANGE='hashchange';
    const ON_LOAD='load';
    const ON_MESSAGE='message';
    const ON_OFFLINE='offline';
    const ON_ON_LINE='online';
    const ON_PAGEHIDE='pagehide';
    const ON_PAGESHOW='pageshow';
    const ON_POPSTATE='popstate';
    const ON_RESIZE='resize';
    const ON_STORAGE='storage';
    const ON_UNLOAD='unload';
    const ON_BLUR='blur';
    const ON_CHANGE='change';
    const ON_CON_TEXTMENU='contextmenu';
    const ON_FOCUS='focus';
    const ON_INPUT='input';
    const ON_INVALID='invalid';
    const ON_RESET='reset';
    const ON_SEARCH='search';
    const ON_SELECT='select';
    const ON_SUBMIT='submit';
    const ON_KEYDOWN='keydown';
    const ON_KEYPRESS='keypress';
    const ON_KEYUP='keyup';
    const ON_CLICK='click';
    const ON_DBLCLICK='dblclick';
    const ON_MOUSEDOWN='mousedown';
    const ON_MOUSEMOVE='mousemove';
    const ON_MOUSEOUT='mouseout';
    const ON_MOUSEOVER='mouseover';
    const ON_MOUSEUP='mouseup';
    const ON_MOUSEWHEEL='mousewheel';
    const ON_WHEEL='wheel';
    const ON_DRAG='drag';
    const ON_DRAGEND='dragend';
    const ON_DRAGCENTER='dragenter';
    const ON_DRAGLEAVE='dragleave';
    const ON_DRAGOVER='dragover';
    const ON_DRAGSTART='dragstart';
    const ON_DROP='drop';
    const ON_SCROLL='scroll';
    const ON_COPY='copy';
    const ON_CUT='cut';
    const ON_PASTE='paste';
    const ON_ABORT='abort';
    const ON_CANPLAY='canplay';
    const ON_CANPLAYTHROUGH='canplaythrough';
    const ON_CUECHANGE='cuechange';
    const ON_DURATION_CHANGE='durationchange';
    const ON_EMPTIED='emptied';
    const ON_ENDED='ended';
    const ON_ERROR='error';
    const ON_LOADEDDATA='loadeddata';
    const ON_LOADEDMETADATA='loadedmetadata';
    const ON_LOADSTART='loadstart';
    const ON_PAUSE='pause';
    const ON_PLAY='play';
    const ON_PLAYING='playing';
    const ON_PROGRESS='progress';
    const ON_RATECHANGE='ratechange';
    const ON_SEEKED='seeked';
    const ON_SEEKING='seeking';
    const ON_STALLED='stalled';
    const ON_SUSPEND='suspend';
    const ON_TIMEUPDATE='timeupdate';
    const ON_VOLUMECHANGE='volumechange';
    const ON_WAITING='waiting';
    const ON_TOGGLECON_ST='toggleconst';

    /**
     * @var string
     */
    protected $target;

    /**
     * @var string
     */
    protected $event;

    /**
     * Interaction constructor.
     */
    protected function __construct($target, $event = self::ON_CLICK)
    {
        $this->target = $target;

        if (!$this->isValueInConstants($event)) {
            throw new NotAllowedValueException(sprintf('Parameter "event" has an not allowed value. Possible constants are %s. Value "%s" given.',$this->getConstantNames(),$event));
        }

        $this->event = $event;
    }

    /**
     * @param $target
     * @param string $event
     * @return Interaction
     * @throws NotAllowedValueException
     */
    public static function create($target, $event = self::ON_CLICK)
    {
        return new static($target,$event);
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }
}
