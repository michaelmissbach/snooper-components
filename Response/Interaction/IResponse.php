<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Interaction;

use Snooper\Components\Provider\IProvider;

/**
 * Interface IResponse
 * @package Snooper\Components\Response\Interaction
 */
interface IResponse
{
    /**
     * @return Interaction
     */
    public function getInteraction();

    /**
     * @return string
     */
    public function getJavascriptMethodName();

    /**
     * @return mixed
     */
    public function getJavascriptParameter();

    /**
     * @return array
     */
    public function getOptions();
}
