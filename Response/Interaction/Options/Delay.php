<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Interaction\Options;

/**
 * Class Delay
 * @package Snooper\Components\Response\Interaction\Options
 */
class Delay implements IInteractionOption
{
    /**
     * @var int
     */
    protected $delay;

    /**
     * @return string
     */
    public function getJsConfigKey()
    {
        return 'delay';
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->delay;
    }

    /**
     * InteractionDelay constructor.
     * @param $delay
     */
    protected function __construct($delay)
    {
        $this->delay = (int)$delay;
    }

    /**
     * @param $delay
     * @return Delay
     */
    public static function create($delay)
    {
        return new static($delay);
    }
}
