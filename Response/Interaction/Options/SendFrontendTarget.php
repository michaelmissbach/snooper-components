<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Interaction\Options;

use Snooper\Components\Exception\NotAllowedValueException;
use Snooper\Components\Utility\Traits\ConstantFinderTrait;

/**
 * Class SendFrontendTarget
 * @package Snooper\Components\Response\Interaction\Options
 */
class SendFrontendTarget implements IInteractionOption
{
    use ConstantFinderTrait;

    /**
     * @var array
     */
    protected $__constants = [
        'SEND_FE_TARGET_NONE'=>'none',
        'SEND_FE_TARGET_FIRST'=>'first',
        'SEND_FE_TARGET_LAST'=>'last'
    ];

    const SEND_FE_TARGET_NONE = 'none';

    const SEND_FE_TARGET_FIRST = 'first';

    const SEND_FE_TARGET_LAST = 'last';

    /**
     * @var string
     */
    protected $sendFeTargetPosition;

    /**
     * @return string
     */
    public function getJsConfigKey()
    {
        return 'sendFeTargetPostion';
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->sendFeTargetPosition;
    }

    /**
     * SendFrontendTarget constructor.
     * @param $sendFeTargetPosition
     * @throws NotAllowedValueException
     * @throws \ReflectionException
     */
    protected function __construct($sendFeTargetPosition)
    {
        if (!$this->isValueInConstants($sendFeTargetPosition)) {
            throw new NotAllowedValueException(sprintf('Parameter "sendFeTargetPosition" has an not allowed value. Possible constants are %s. Value "%s" given.',$this->getConstantNames(),$sendFeTargetPosition));
        }

        $this->sendFeTargetPosition = $sendFeTargetPosition;
    }

    /**
     * @param $sendFeTargetPosition
     * @return SendFrontendTarget
     * @throws NotAllowedValueException
     * @throws \ReflectionException
     */
    public static function create($sendFeTargetPosition)
    {
        return new static($sendFeTargetPosition);
    }
}
