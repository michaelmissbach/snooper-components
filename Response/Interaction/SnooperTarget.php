<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Interaction;

/**
 * Class SnooperTarget
 * @package Snooper\Components\Response\Interaction
 */
class SnooperTarget extends Interaction
{
    const PATTERN = '[data-snooper-event=\'%s\']';

    /**
     * Interaction constructor.
     */
    protected function __construct($target, $event = self::ON_CLICK)
    {
        parent::__construct($target,$event);

        $this->target = sprintf(self::PATTERN,$target);
    }
}
