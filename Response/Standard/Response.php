<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Standard;

use Snooper\Components\Provider\IProvider;
use Snooper\Components\Response\ICommonResponse;
use Snooper\Components\Response\Parameters\IDeliver;
use Snooper\Components\Response\Parameters\Parameter;
use Snooper\Components\Snooper;

/**
 * Class Response
 * @package Snooper\Components\Response\Standard
 */
class Response implements ICommonResponse,IResponse
{
    /**
     * @var string
     */
    protected $source;

    /**
     * @var string
     */
    protected $javascriptMethodName;

    /**
     * @var mixed
     */
    protected $javascriptParameter;

    /**
     * @var boolean
     */
    protected $delivered = false;

    /**
     * @var IDeliver
     */
    protected $deliver;

    /**
     * Response constructor.
     * @param IDeliver $deliver
     * @param $javascriptMethodName
     * @param null $javascriptParameter
     * @throws \Exception
     */
    protected function __construct(IDeliver $deliver, $javascriptMethodName, $javascriptParameter = null)
    {
        $this->deliver = $deliver;
        $this->javascriptMethodName     = $javascriptMethodName;

        if ($javascriptParameter !== null) {
            if ($javascriptParameter instanceof Parameter) {
                $this->javascriptParameter = $javascriptParameter;
            } else {
                $this->javascriptMethodName = "Snooper.log";
                $this->javascriptParameter = Parameter::createFromList(
                    sprintf(
                        'Snooper error: The parameter "javascriptParameter" on method call "%s" has to be an instance of Snooper\Components\Response\Parameters\IParameter.',
                        $javascriptMethodName
                    )
                    ,false
                );
            }
        }
    }

    /**
     * @param IDeliver $deliver
     * @param $javascriptMethodName
     * @param null $javascriptParameter
     * @return Response
     * @throws \Exception
     */
    public static function create(IDeliver $deliver, $javascriptMethodName, $javascriptParameter = null)
    {
        return new static($deliver,$javascriptMethodName,$javascriptParameter);
    }

    /**
     * @param IProvider $provider
     */
    public function setProvider(IProvider $provider)
    {
        $this->source = get_class($provider);
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function getJavascriptMethodName()
    {
        return $this->javascriptMethodName;
    }

    /**
     * @return mixed
     */
    public function getJavascriptParameter()
    {
        return $this->javascriptParameter;
    }

    /**
     * @return bool
     */
    public function isDelivered()
    {
        return $this->delivered;
    }

    /**
     * @param bool $delivered
     * @return Response
     */
    public function setDelivered($delivered)
    {
        $this->delivered = $delivered;
        return $this;
    }

    /**
     * @return IDeliver
     */
    public function getDeliver()
    {
        return $this->deliver;
    }

    /**
     * @return mixed
     */
    public function prepare()
    {
        $return = [];

        $return['type'] = 'standard';

        $return['method'] = $this->getJavascriptMethodName();

        if ($this->getJavascriptParameter() instanceof Parameter) {
            $return['params'] = $this->getJavascriptParameter()->getArguments();
        } else {
            $return['params'] = null;
        }

        return $return;
    }

    /**
     * @return array|string
     */
    public function getSendResponseLog()
    {
        return sprintf('Add response ("%s").',$this->getJavascriptMethodName());
    }
}
