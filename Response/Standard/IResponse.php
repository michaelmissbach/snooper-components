<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Standard;

use Snooper\Components\Response\Parameters\IDeliver;

/**
 * Interface IResponse
 * @package Snooper\Components\Response\Standard
 */
interface IResponse
{
    /**
     * @return string
     */
    public function getJavascriptMethodName();

    /**
     * @return mixed
     */
    public function getJavascriptParameter();
}
