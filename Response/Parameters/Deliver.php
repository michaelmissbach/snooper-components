<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Parameters;

use Snooper\Components\Exception\NotAllowedValueException;
use Snooper\Components\Utility\Traits\ConstantFinderTrait;

/**
 * Class Deliver
 * @package Snooper\Components\Response\Parameters
 */
class Deliver implements IDeliver
{
    use ConstantFinderTrait;

    /**
     * @var array
     */
    protected $__constants = [
        'PRE_SECTION'=>1,
        'POST_SECTION'=>2,
        'LAST_SECTION'=>3
    ];

    const PRE_SECTION   = 1;

    const POST_SECTION  = 2;

    const LAST_SECTION = 3;

    /**
     * @var string
     */
    protected $section = '';

    /**
     * @var integer
     */
    protected $deliverTimeOfExecution;

    /**
     * Deliver constructor.
     * @param string $section
     * @param int $deliverTimeOfExecution
     * @throws \Exception
     */
    protected function __construct($section = '', $deliverTimeOfExecution = self::POST_SECTION)
    {
        $this->section = $section;

        if (!$this->isValueInConstants($deliverTimeOfExecution)) {
            throw new NotAllowedValueException(sprintf('Parameter "deliverTimeOfExecution" has an not allowed value. Possible constants are %s. Value "%s" given.',$this->getConstantNames(),$deliverTimeOfExecution));
        }

        $this->deliverTimeOfExecution   = $deliverTimeOfExecution;
    }

    /**
     * @param string $section
     * @param int $deliverTimeOfExecution
     * @return Deliver
     * @throws \Exception
     */
    public static function create($section = '', $deliverTimeOfExecution = self::POST_SECTION)
    {
        return new static($section,$deliverTimeOfExecution);
    }

    /**
     * @return string
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @return int
     */
    public function getDeliverTimeOfExecution()
    {
        return $this->deliverTimeOfExecution;
    }
}
