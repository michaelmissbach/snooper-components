<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Parameters;

/**
 * Interface IParameter
 * @package Snooper\Components\Response\Parameters
 */
interface IParameter
{
    /**
     * @return array
     */
    public function getArguments();

    /**
     * @param mixed $args
     * @return Parameter
     * @throws \Exception
     */
    public static function createFromArray($args);

    /**
     * @param mixed ...$args
     * @return Parameter
     * @throws \Exception
     */
    public static function createFromList(...$args);
}
