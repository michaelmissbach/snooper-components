<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Parameters;

use Snooper\Components\Exception\ParameterException;

/**
 * Class Parameter
 * @package Snooper\Components\Response\Parameters
 */
class Parameter implements IParameter
{
    /**
     * @var array
     */
    protected $arguments = array();

    /**
     * Parameter constructor.
     * @param mixed $args
     * @throws \Exception
     */
    protected function __construct($args)
    {
        if (count($args) === 1) {
            $args[1] = false;
        }

        if (!count($args) %2 || count($args) === 0) {
            throw new ParameterException('Illegal parameter count.');
        }

        for ($i = 0; $i < count($args); $i=$i+2) {
            $isRawEvalNeededParam = isset($args[$i+1]) ? $args[$i+1] : false;
            if (!is_bool($isRawEvalNeededParam)) {
                throw new ParameterException('The parameter isRawEvalNeededParam has to be a boolean.');
            }
            $this->arguments[] = ['param'=>$args[$i],'isRawEvalNeededParam'=>$args[$i+1]];
        }
    }

    /**
     * @param mixed $args
     * @return Parameter
     * @throws \Exception
     */
    public static function createFromArray($args)
    {
        return new static($args);
    }

    /**
     * @param mixed ...$args
     * @return Parameter
     * @throws \Exception
     */
    public static function createFromList(...$args)
    {
        return new static($args);
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }
}
