<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response\Raw;

use Snooper\Components\Provider\IProvider;
use Snooper\Components\Response\ICommonResponse;
use Snooper\Components\Response\Parameters\IDeliver;
use Snooper\Components\Response\Parameters\Parameter;
use Snooper\Components\Snooper;

/**
 * Class Response
 * @package Snooper\Components\Response\Raw
 */
class Response implements ICommonResponse,IResponse
{
    /**
     * @var string
     */
    protected $source;

    /**
     * @var string
     */
    protected $raw;

    /**
     * @var boolean
     */
    protected $delivered = false;

    /**
     * @var IDeliver
     */
    protected $deliver;

    /**
     * Response constructor.
     * @param IDeliver $deliver
     * @param $raw
     * @throws \Exception
     */
    protected function __construct(IDeliver $deliver, $raw)
    {
        $this->deliver = $deliver;
        $this->raw     = $raw;
    }

    /**
     * @param IDeliver $deliver
     * @param $raw
     * @return Response
     * @throws \Exception
     */
    public static function create(IDeliver $deliver, $raw)
    {
        return new static($deliver,$raw);
    }

    /**
     * @param IProvider $provider
     */
    public function setProvider(IProvider $provider)
    {
        $this->source = get_class($provider);
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * @return mixed
     */
    public function getJavascriptParameter()
    {
        return '';
    }

    /**
     * @return bool
     */
    public function isDelivered()
    {
        return $this->delivered;
    }

    /**
     * @param bool $delivered
     * @return Response
     */
    public function setDelivered($delivered)
    {
        $this->delivered = $delivered;
        return $this;
    }

    /**
     * @return IDeliver
     */
    public function getDeliver()
    {
        return $this->deliver;
    }

    /**
     * @return mixed
     */
    public function prepare()
    {
        $return = [];

        $return['type'] = 'raw';

        $return['raw'] = $this->getRaw();

        if ($this->getJavascriptParameter() instanceof Parameter) {
            $return['params'] = $this->getJavascriptParameter()->getArguments();
        } else {
            $return['params'] = null;
        }

        return $return;
    }

    /**
     * @return array|string
     */
    public function getSendResponseLog()
    {
        return sprintf('Add response ("%s").',$this->getRaw());
    }
}
