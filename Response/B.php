<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response;

use Snooper\Components\Response\Interaction\FreeSelector;
use Snooper\Components\Response\Interaction\IInteraction;
use Snooper\Components\Response\Interaction\Options\Delay;
use Snooper\Components\Response\Interaction\Options\SendFrontendTarget;
use Snooper\Components\Response\Interaction\SnooperTarget;
use Snooper\Components\Response\Parameters\Deliver;
use Snooper\Components\Response\Parameters\IDeliver;
use Snooper\Components\Response\Parameters\Parameter;

/**
 * Class B
 * @package Snooper\Components\Response
 */
class B extends Builder
{
    /**
     * @param IDeliver $deliver
     * @param $javascriptMethodName
     * @param null $javascriptParameter
     * @return Standard\Response
     */
    public static function csr(IDeliver $deliver, $javascriptMethodName, $javascriptParameter = null)
    {
        return Builder::createStandardResponse($deliver,$javascriptMethodName,$javascriptParameter);
    }

    /**
     * @param IDeliver $deliver
     * @param $javascriptVariableName
     * @param null $javascriptParameter
     * @return Variable\Response
     * @throws \Exception
     */
    public static function cvs(IDeliver $deliver, $javascriptVariableName, $javascriptParameter = null)
    {
        return Builder::createVariableSet($deliver, $javascriptVariableName, $javascriptParameter);
    }

    /**
     * @param IDeliver $deliver
     * @param $raw
     * @return Raw\Response
     * @throws \Exception
     */
    public static function cr(IDeliver $deliver,$raw)
    {
        return Builder::createRaw($deliver,$raw);
    }

    /**
     * @param string $section
     * @param int $deliverTimeOfExecution
     * @return Deliver
     */
    public static function cd($section = '', $deliverTimeOfExecution = Deliver::POST_SECTION)
    {
        return Builder::createDeliver($section,$deliverTimeOfExecution);
    }

    /**
     * @param IInteraction $interaction
     * @param $javascriptMethodName
     * @param null $javascriptParameter
     * @param $options
     * @return Interaction\Response
     */
    public static function cir(IInteraction $interaction,$javascriptMethodName, $javascriptParameter = null, ...$options)
    {
        return Interaction\Response::create($interaction,$javascriptMethodName,$javascriptParameter,$options);
    }

    /**
     * @param $target
     * @param string $event
     * @return SnooperTarget
     */
    public static function cist($target,$event = Interaction\Interaction::ON_CLICK)
    {
        return Builder::createInteractionSnooperTarget($target,$event);
    }

    /**
     * @param $target
     * @param string $event
     * @return FreeSelector
     */
    public static function cifs($target,$event = Interaction\Interaction::ON_CLICK)
    {
        return Builder::createInteractionFreeSelector($target,$event);
    }

    /**
     * @param $milliseconds
     * @return Delay
     */
    public static function ciod($milliseconds)
    {
        return Builder::createInteractionOptionDelay($milliseconds);
    }

    /**
     * @param string $sendFeTargetPosition
     * @return SendFrontendTarget
     */
    public static function ciosft($sendFeTargetPosition = SendFrontendTarget::SEND_FE_TARGET_FIRST)
    {
        return Builder::createInteractionOptionSendFrontendTarget($sendFeTargetPosition);
    }

    /**
     * @return Interaction\Options\Remove
     */
    public static function cior()
    {
        return Builder::createInteractionOptionRemove();
    }

    /**
     * @param mixed ...$args
     * @return Parameter
     */
    public static function cp(...$args)
    {
        return Parameter::createFromArray($args);
    }
}
