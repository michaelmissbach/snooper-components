<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response;

use Snooper\Components\Response\Interaction\FreeSelector;
use Snooper\Components\Response\Interaction\IInteraction;
use Snooper\Components\Response\Interaction\Options\Delay;
use Snooper\Components\Response\Interaction\Options\Remove;
use Snooper\Components\Response\Interaction\Options\SendFrontendTarget;
use Snooper\Components\Response\Interaction\SnooperTarget;
use Snooper\Components\Response\Parameters\Deliver;
use Snooper\Components\Response\Parameters\IDeliver;
use Snooper\Components\Response\Parameters\Parameter;

/**
 * Class Builder
 * @package Snooper\Components\Response
 */
class Builder
{
    /**
     * @param IDeliver $deliver
     * @param $javascriptMethodName
     * @param null $javascriptParameter
     * @return Standard\Response
     */
    public static function createStandardResponse(IDeliver $deliver, $javascriptMethodName, $javascriptParameter = null)
    {
        return Standard\Response::create($deliver,$javascriptMethodName,$javascriptParameter);
    }

    /**
     * @param IDeliver $deliver
     * @param $javascriptVariableName
     * @param null $javascriptParameter
     * @return Variable\Response
     */
    public static function createVariableSet(IDeliver $deliver, $javascriptVariableName, $javascriptParameter = null)
    {
        return Variable\Response::create($deliver, $javascriptVariableName, $javascriptParameter);
    }

    /**
     * @param IDeliver $deliver
     * @param $raw
     * @return Raw\Response
     */
    public static function createRaw(IDeliver $deliver, $raw)
    {
        return Raw\Response::create($deliver, $raw);
    }


    /**
     * @param string $section
     * @param int $deliverTimeOfExecution
     * @return Deliver
     */
    public static function createDeliver($section = '', $deliverTimeOfExecution = Deliver::POST_SECTION)
    {
        return Deliver::create($section,$deliverTimeOfExecution);
    }



    /**
     * @param IInteraction $interaction
     * @param $javascriptMethodName
     * @param null $javascriptParameter
     * @param $options
     * @return Interaction\Response
     */
    public static function createInteractionResponse(IInteraction $interaction,$javascriptMethodName, $javascriptParameter = null, ...$options)
    {
        return Interaction\Response::create($interaction,$javascriptMethodName,$javascriptParameter,$options);
    }

    /**
     * @param $target
     * @param string $event
     * @return SnooperTarget
     */
    public static function createInteractionSnooperTarget($target,$event = Interaction\Interaction::ON_CLICK)
    {
        return SnooperTarget::create($target,$event);
    }

    /**
     * @param $target
     * @param string $event
     * @return FreeSelector
     */
    public static function createInteractionFreeSelector($target,$event = Interaction\Interaction::ON_CLICK)
    {
        return FreeSelector::create($target,$event);
    }

    /**
     * @param $milliseconds
     * @return Delay
     */
    public static function createInteractionOptionDelay($milliseconds)
    {
        return Delay::create($milliseconds);
    }

    /**
     * @param string $sendFeTargetPosition
     * @return SendFrontendTarget
     */
    public static function createInteractionOptionSendFrontendTarget($sendFeTargetPosition = SendFrontendTarget::SEND_FE_TARGET_FIRST)
    {
        return SendFrontendTarget::create($sendFeTargetPosition);
    }

    /**
     * @return Remove
     */
    public static function createInteractionOptionRemove()
    {
        return Remove::create();
    }


    /**
     * @param mixed ...$args
     * @return Parameter
     */
    public static function createParameter(...$args)
    {
        return Parameter::createFromArray($args);
    }
}
