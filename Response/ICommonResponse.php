<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Response;

use Snooper\Components\Provider\IProvider;
use Snooper\Components\Response\Parameters\IDeliver;
use Snooper\Components\Response\Raw\IResponse;

/**
 * Interface ICommonResponse
 * @package Snooper\Components\Response
 */
interface ICommonResponse
{
    /**
     * @param IProvider $provider
     */
    public function setProvider(IProvider $provider);

    /**
     * @return string
     */
    public function getSource();

    /**
     * @return mixed
     */
    public function prepare();

    /**
     * @return bool
     */
    public function isDelivered();

    /**
     * @param bool $delivered
     * @return IResponse
     */
    public function setDelivered($delivered);

    /**
     * @return IDeliver
     */
    public function getDeliver();

    /**
     * @return string
     */
    public function getSendResponseLog();
}