<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Renderer;

use Snooper\Components\Compiler\Compiler;
use Snooper\Components\Log\ILog;
use Snooper\Components\Log\LoggerContainer;

/**
 * Class Renderer
 * @package Snooper\Components\Renderer
 */
class Renderer implements IRenderer
{
    /**
     * @var array
     */
    protected $loadedTemplates = [];

    /**
     * @return bool|string
     */
    protected function getBasePath()
    {
        return realpath(__DIR__.'/../Resources/templates/');
    }

    /**
     * @param $file
     * @param $markers
     * @return bool|mixed|string
     */
    public function render($file,$markers = [])
    {
        $file = sprintf('%s/%s',$this->getBasePath(),$file);

        LoggerContainer::instance()->add(Compiler::LOG_COMPILER_KEY,sprintf('Starting render (%s).',$file), ILog::BLOCK_BEGIN);

        if (!array_key_exists($file, $this->loadedTemplates)) {
            LoggerContainer::instance()->add(Compiler::LOG_COMPILER_KEY,sprintf('Load file (%s).',$file));
            $this->loadedTemplates[$file] = file_get_contents($file);
        }

        $content = str_replace(array_keys($markers),array_values($markers),$this->loadedTemplates[$file]);

        LoggerContainer::instance()->add(Compiler::LOG_COMPILER_KEY,sprintf('Finish render (%s).',$file), ILog::BLOCK_END);

        return $content;
    }
}
