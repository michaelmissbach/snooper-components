<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Log;

/**
 * Interface ILog
 * @package Snooper\Components\Log
 */
interface ILog
{
    const BLOCK_BEGIN   = 'begin';

    const BLOCK_CONTENT = 'content';

    const BLOCK_END     = 'end';

    /**
     * @return int
     */
    public function getExecutionTime();

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return string
     */
    public function getType();
}
