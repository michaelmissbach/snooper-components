<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Log;

use Snooper\Components\Log\ILog;
use Snooper\Components\Log\Log;
use Snooper\Components\Utility\Traits\Container\Container;
use Snooper\Components\Utility\Traits\Container\ContainerGrouped;
use Snooper\Components\Utility\Traits\Singleton;

/**
 * Class LoggerContainer
 * @package Snooper\Components\Container
 */
class LoggerContainer
{
    use Singleton;
    use Container;
    use ContainerGrouped;

    /**
     * @var
     */
    protected static $startExecutionTime;

    /**
     * @var bool
     */
    protected $enabled = false;

    /**
     * @var int
     */
    protected $logCount = 0;

    /**
     * LoggerContainer constructor.
     */
    protected function __construct()
    {
        self::$startExecutionTime = microtime(true);
    }

    /**
     * @param $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return int
     */
    public function getLogCount()
    {
        return $this->logCount;
    }

    /**
     * @param int $logCount
     */
    public function setLogCount(int $logCount)
    {
        $this->logCount = $logCount;
    }

    /**
     * @param $message
     * @param string $type
     */
    public function add($group,$message,$type = ILog::BLOCK_CONTENT)
    {
        if ($this->enabled) {
            $this->createGroup($group);

            $this->container[$group][] = new Log($message, microtime(true) - static::$startExecutionTime, $type);
            $this->logCount++;
        }
    }

    /**
     *
     */
    public function clear()
    {
        $this->container = [];
        $this->count = 0;
        $this->logCount = 0;
    }
}