<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Log;

/**
 * Class Log
 * @package Snooper\Components\Log
 */
class Log implements ILog
{
    /**
     * @var integer
     */
    protected $executionTime;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $type;

    /**
     * Log constructor.
     * @param $message
     * @param $executionTime
     * @param string $type
     */
    public function __construct($message,$executionTime,$type)
    {
        $this->message          = $message;
        $this->executionTime    = round($executionTime * 1000000);
        $this->type             = $type;
    }

    /**
     * @return int
     */
    public function getExecutionTime()
    {
        return $this->executionTime;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
