<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components;

use Snooper\Components\Compiler\CompiledContainer;
use Snooper\Components\Compiler\Compiler;
use Snooper\Components\Compiler\ICompiler;
use Snooper\Components\Config\Config;
use Snooper\Components\Config\IConfig;
use Snooper\Components\Serialize\Serializer;
use Snooper\Components\Serialize\ISerializer;
use Snooper\Components\EventTrigger\EventTrigger;
use Snooper\Components\Log\LoggerContainer;
use Snooper\Components\Provider\ProviderCallContainer;
use Snooper\Components\Provider\ProviderContainer;
use Snooper\Components\Controller\Controller;
use Snooper\Components\Controller\IController;
use Snooper\Components\Processor\IProcessor;
use Snooper\Components\Processor\Processor;
use Snooper\Components\Provider\IProvider;
use Snooper\Components\Renderer\IRenderer;
use Snooper\Components\Renderer\Renderer;
use Snooper\Components\Utility\Traits\Singleton;

/**
 * Class Snooper
 * @package Snooper\Components
 */
final class Snooper
{
    use Singleton;

    const VERSION_MAJOR = 1;

    const VERSION_MINOR = 5;

    const VERSION_REV = 0;

    const VERSION_ID = 10500;

    const COPYRIGHT_YEAR = '2020';

    /**
     * @return string
     */
    public static function getVersion()
    {
        return sprintf('%s.%s.%s', self::VERSION_MAJOR, self::VERSION_MINOR,
            self::VERSION_REV);
    }

    /**
     * @var IController
     */
    protected $controller;

    /**
     * @var IConfig
     */
    protected $config;

    /**
     * @var IProcessor
     */
    protected $processor;

    /**
     * @var ICompiler
     */
    protected $compiler;

    /**
     * @var IRenderer
     */
    protected $renderer;

    /**
     * @var ISerializer
     */
    protected $serializer;

    /**
     * @var mixed
     */
    protected $context;

    /**
     * only for internal uses
     * @internal
     */
    public function reset()
    {
        self::$_instance = null;
        ProviderContainer::instance()->clear();
        ProviderCallContainer::instance()->clear();
        CompiledContainer::instance()->clear();
        Response\Container::instance()->clear();
    }

    /**
     * @param IProvider $provider
     */
    public function addProvider(IProvider $provider)
    {
        ProviderContainer::instance()->add($provider);
    }

    /**
     * @param $name
     * @param $params
     */
    public function raiseEvent($name, $params)
    {
        try {

            $e = EventTrigger::create($name,$params);

            $this->getController()->onRaiseEvent($e);
            $this->getController()->onRaiseEventExecution($e);

        }catch(\Exception $e) {

            LoggerContainer::instance()->add('Main',$e->getMessage());

        }
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        if (!$this->config) {
            $this->config = new Config();
        }
        return $this->config;
    }

    /**
     * @return Controller
     */
    public function getController()
    {
        if (!$this->controller) {
            $this->controller = new Controller();
        }
        return $this->controller;
    }

    /**
     * @return Processor
     */
    public function getProcessor()
    {
        if (!$this->processor) {
            $this->processor = new Processor();
        }
        return $this->processor;
    }

    /**
     * @return Renderer
     */
    public function getRenderer()
    {
        if (!$this->renderer) {
            $this->renderer = new Renderer();
        }
        return $this->renderer;
    }

    /**
     * @return Compiler
     */
    public function getCompiler()
    {
        if (!$this->compiler) {
            $this->compiler = new Compiler();
        }
        return $this->compiler;
    }

    /**
     * @return ISerializer
     */
    public function getSerializer()
    {
        if (!$this->serializer) {
            $this->serializer = new Serializer();
        }
        return $this->serializer;
    }

    /**
     * @param IController $controller
     * @return Snooper
     */
    public function setController(IController $controller)
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @param IConfig $config
     * @return Snooper
     */
    public function setConfig(IConfig $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @param IProcessor $processor
     * @return Snooper
     */
    public function setProcessor(IProcessor $processor)
    {
        $this->processor = $processor;
        return $this;
    }

    /**
     * @param ICompiler $compiler
     * @return Snooper
     */
    public function setCompiler(ICompiler $compiler)
    {
        $this->compiler = $compiler;
        return $this;
    }

    /**
     * @param IRenderer $renderer
     * @return Snooper
     */
    public function setRenderer(IRenderer $renderer)
    {
        $this->renderer = $renderer;
        return $this;
    }

    /**
     * @param ISerializer $serializer
     * @return $this
     */
    public function setSerializer(ISerializer $serializer)
    {
        $this->serializer = $serializer;
        return $this;
    }

    /**
     * @return \stdClass
     */
    public function getContext()
    {
        if (!$this->context) {
            $this->context = new \stdClass();
        }
        return $this->context;
    }

    /**
     * @param $context
     * @return Snooper
     */
    public function setContext($context)
    {
        $this->context = $context;
        return $this;
    }

    /**
     * @param $section
     * @return string
     * @throws \Exception
     */
    public function outputSection($section)
    {
        return $this->getController()->outputSection($section);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getDeliverResponses()
    {
        $list = [];
        foreach (Response\Container::instance()->getRaw() as &$response) {
            if (!$response->isDelivered()) {
                $list[] = $response->prepare();
                $response->setDelivered(true);
            }
        }

        return $list;
    }

    /**
     * @return int
     */
    public function getUndeliveredResponseCount()
    {
        $counter = 0;
        foreach (Response\Container::instance()->getRaw() as &$response) {
            if (!$response->isDelivered()) {
                $counter++;
            }
        }
        return $counter;
    }
}
