<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Config;

/**
 * Interface IConfig
 * @package Snooper\Components\Config
 */
interface IConfig
{
    /**
     * @param $config
     */
    public function mergeConfig($config);

    /**
     * @return mixed
     */
    public function getFirstSection();

    /**
     * @return mixed
     */
    public function getLastSection();

    /**
     * @return mixed
     */
    public function getAvailableSections();

    /**
     * @return mixed
     */
    public function getDebugUrlParam();

    /**
     * @return mixed
     */
    public function getDebugNestedLevel();

    /**
     * @return mixed
     */
    public function getDebugSessionParam();

    /**
     * @return mixed
     */
    public function getDebugEnvironments();

    /**
     * @return bool
     */
    public function isDebugMode();

    /**
     * @param bool $debugMode
     * @return Config
     */
    public function setDebugMode($debugMode);

    /**
     * @return mixed
     */
    public function getLogExcludedClasses();

    /**
     * @return mixed
     */
    public function getJsonResponseKey();

    /**
     * @return mixed
     */
    public function getXhrSendMode();

    /**
     * @return mixed
     */
    public function getIgnoredRoutes();
}