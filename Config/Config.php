<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Config;

/**
 * Class Config
 * @package Snooper\Components\Config
 */
class Config implements IConfig
{
    const XHR_SEND_METHOD_HEADER    = 'header';
    const XHR_SEND_METHOD_BODY      = 'body';
    const XHR_SEND_METHOD_BOTH      = 'both';
    
    const DEBUG = 'debug';

    /**
     * initial config
     *
     * @var array
     */
    protected $rawParameters = [
        'first_section' => 'head',
        'last_section' => 'footer',
        'sections' => [
            'head','body','footer'
        ],
        'json_response_key' => 'snooper_data',
        'xhr_send_mode'=>self::XHR_SEND_METHOD_BOTH,
        self::DEBUG => [
            'nested_level' => 25,
            'url_param' => 'snooper_debug',
            'session_param' => 'snooper_debug',
            'debug_environments' => [
                'dev'
            ]
        ],
        'log' => [
            'excludeClass' => [
                'Doctrine\ORM\PersistentCollection'
            ]
        ],
        'ignoredRoutes' => [
            '/\/admin$/'
        ]
    ];

    /**
     * @var bool
     */
    protected $debugMode = false;

    /**
     * @param $config
     */
    public function mergeConfig($config)
    {
        $this->rawParameters = array_merge($this->rawParameters,$config);
    }

    /**
     * @return mixed
     */
    public function getFirstSection()
    {
        return $this->rawParameters['first_section'];
    }

    /**
     * @return mixed
     */
    public function getLastSection()
    {
        return $this->rawParameters['last_section'];
    }

    /**
     * @return mixed
     */
    public function getAvailableSections()
    {
        return $this->rawParameters['sections'];
    }

    /**
     * @return mixed
     */
    public function getDebugNestedLevel()
    {
        return $this->rawParameters[self::DEBUG]['nested_level'];
    }

    /**
     * @return mixed
     */
    public function getDebugUrlParam()
    {
        return $this->rawParameters[self::DEBUG]['url_param'];
    }

    /**
     * @return mixed
     */
    public function getDebugSessionParam()
    {
        return $this->rawParameters[self::DEBUG]['session_param'];
    }

    /**
     * @return mixed
     */
    public function getDebugEnvironments()
    {
        return $this->rawParameters[self::DEBUG]['debug_environments'];
    }

    /**
     * @return bool
     */
    public function isDebugMode()
    {
        return $this->debugMode;
    }

    /**
     * @param bool $debugMode
     * @return Config
     */
    public function setDebugMode($debugMode)
    {
        $this->debugMode = $debugMode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogExcludedClasses()
    {
        return $this->rawParameters['log']['excludeClass'];
    }

    /**
     * @return mixed
     */
    public function getJsonResponseKey()
    {
        return $this->rawParameters['json_response_key'];
    }

    /**
     * @return mixed
     */
    public function getXhrSendMode()
    {
        $method = $this->rawParameters['xhr_send_mode'];
        return ($method === self::XHR_SEND_METHOD_BOTH ||
            $method === self::XHR_SEND_METHOD_HEADER ||
            $method === self::XHR_SEND_METHOD_BODY)
            ?
            $method : self::XHR_SEND_METHOD_BOTH;
    }

    /**
     * @return mixed
     */
    public function getIgnoredRoutes()
    {
        return $this->rawParameters['ignoredRoutes'];
    }
}
