<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Fixtures\Utility\Traits;

use Snooper\Components\Utility\Traits\Singleton;

/**
 * Class SingletonFixture
 * @package Snooper\Components\Tests\Fixtures\Utility\Traits
 * @internal
 */
class SingletonFixture
{
    use Singleton;
}
