<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Fixtures\Provider;

use Snooper\Components\EventTrigger\IEventTrigger;
use Snooper\Components\Provider\ProviderCallDefinition;
use Snooper\Components\Response\Builder;

/**
 * Class ProviderPreAndPostEvents
 * @package Snooper\Components\Tests\Fixtures\Provider
 * @internal
 */
class ProviderPreAndPostEvents extends \Snooper\Components\Provider\Provider
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return true;
    }

    /**
     * @return array
     */
    public function getEvents()
    {
        return [
            'event' => ProviderCallDefinition::create('m1',[])
        ];
    }

    public function preRaiseEvent(IEventTrigger $e)
    {
        return [
            Builder::createStandardResponse(
                Builder::createDeliver('head'),
                'console.log',
                Builder::createParameter('hello world from preRaiseEvent')
            )
        ];
    }

    public function postRaiseEvent(IEventTrigger $e)
    {
        return [
            Builder::createStandardResponse(
                Builder::createDeliver('head'),
                'console.log',
                Builder::createParameter('hello world from postRaiseEvent')
            )
        ];
    }

    public function m1($e)
    {
    }
}
