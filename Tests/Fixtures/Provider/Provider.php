<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Fixtures\Provider;

use Snooper\Components\Provider\ProviderCallDefinition;
use Snooper\Components\Response\Builder;

/**
 * Class Provider
 * @package Snooper\Components\Tests\Fixtures\Provider
 * @internal
 */
class Provider extends \Snooper\Components\Provider\Provider
{
    /**
     * @return bool
     */
    public function isActive()
    {
        return true;
    }

    /**
     * @return array
     */
    public function getEvents()
    {
        return [
            'eventStandardResponse' => ProviderCallDefinition::create('m1',['key1'=>'value1','key2'=>'value2']),
            'eventInteractionResponse' => ProviderCallDefinition::create('m2',['key1'=>'value1','key2'=>'value2']),
            'eventMixed' => ProviderCallDefinition::create('m3',['key1'=>'value1','key2'=>'value2'])
        ];
    }

    public function getHeadSection()
    {
        return '<script>console.log("hello world from head");</script>';
    }


    public function m1($e)
    {
        return [
            Builder::createStandardResponse(
                Builder::createDeliver('head'),
                'console.log',
                Builder::createParameter('hello world from m1')
            )
        ];
    }


    public function m2($e)
    {
        return [
            Builder::createInteractionResponse(
                Builder::createInteractionFreeSelector('.test'),
                'console.log',
                Builder::createParameter('hello world from m2')
            )
        ];
    }

    public function m3($e)
    {
        return array_merge(
            $this->m1($e),
            $this->m2($e)
        );
    }
}
