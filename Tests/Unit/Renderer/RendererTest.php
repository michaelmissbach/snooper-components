<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Renderer;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Renderer\Renderer;

/**
 * Class RendererTest
 * @package Snooper\Components\Tests\Unit\Renderer
 */
class RendererTest extends TestCase
{
    public function testRenderPlain()
    {
        $renderer = new class extends Renderer {
            protected function getBasePath()
            {
                return realpath(__DIR__.'/../../Fixtures/Renderer/');
            }
        };
        $this->assertEquals($renderer->render('plain.tpl',[]),'SNOOPER-TEST');
    }

    public function testRenderWithMarker()
    {
        $renderer = new class extends Renderer {
            protected function getBasePath()
            {
                return realpath(__DIR__.'/../../Fixtures/Renderer/');
            }
        };
        $this->assertEquals($renderer->render('marker.tpl',['###SNOOPER###'=>'SNOOPER','###TEST###'=>'TEST']),'SNOOPER-TEST');
    }
}
