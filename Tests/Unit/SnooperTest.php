<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Compiler\Compiler;
use Snooper\Components\Compiler\ICompiler;
use Snooper\Components\Config\Config;
use Snooper\Components\Config\IConfig;
use Snooper\Components\Controller\Controller;
use Snooper\Components\Controller\IController;
use Snooper\Components\Processor\IProcessor;
use Snooper\Components\Processor\Processor;
use Snooper\Components\Renderer\IRenderer;
use Snooper\Components\Renderer\Renderer;
use Snooper\Components\Response\Interaction\FreeSelector;
use Snooper\Components\Response\Parameters\Deliver;
use Snooper\Components\Serialize\ISerializer;
use Snooper\Components\Serialize\Serializer;
use Snooper\Components\Snooper;

/**
 * Class SnooperTest
 * @package Snooper\Components\Tests\Unit
 */
class SnooperTest extends TestCase
{
    public function testGetController()
    {
        Snooper::instance()->reset();
        $this->assertTrue(Snooper::instance()->getController() instanceof IController);
        Snooper::instance()->reset();
    }

    public function testGetConfig()
    {
        Snooper::instance()->reset();
        $this->assertTrue(Snooper::instance()->getConfig() instanceof IConfig);
        Snooper::instance()->reset();
    }

    public function testGetCompiler()
    {
        Snooper::instance()->reset();
        $this->assertTrue(Snooper::instance()->getCompiler() instanceof ICompiler);
        Snooper::instance()->reset();
    }

    public function testGetProcessor()
    {
        Snooper::instance()->reset();
        $this->assertTrue(Snooper::instance()->getProcessor() instanceof IProcessor);
        Snooper::instance()->reset();
    }

    public function testGetRenderer()
    {
        Snooper::instance()->reset();
        $this->assertTrue(Snooper::instance()->getRenderer() instanceof IRenderer);
        Snooper::instance()->reset();
    }

    public function testGetSerializer()
    {
        Snooper::instance()->reset();
        $this->assertTrue(Snooper::instance()->getSerializer() instanceof ISerializer);
        Snooper::instance()->reset();
    }

    public function testGetSetContext()
    {
        Snooper::instance()->reset();
        $this->assertTrue(Snooper::instance()->getContext() instanceof \stdClass);
        $context = new \stdClass();
        Snooper::instance()->setContext($context);
        $this->assertTrue(Snooper::instance()->getContext() instanceof \stdClass);
        Snooper::instance()->reset();
    }

    public function testSetController()
    {
        Snooper::instance()->reset();
        $instance = new class extends Controller implements IController {};
        Snooper::instance()->setController($instance);
        $this->assertEquals(Snooper::instance()->getController(),$instance);
        Snooper::instance()->reset();
    }

    public function testSetConfig()
    {
        Snooper::instance()->reset();
        $instance = new class extends Config implements IConfig {};
        Snooper::instance()->setConfig($instance);
        $this->assertEquals(Snooper::instance()->getConfig(),$instance);
        Snooper::instance()->reset();
    }

    public function testsetCompiler()
    {
        Snooper::instance()->reset();
        $instance = new class extends Compiler implements ICompiler {};
        Snooper::instance()->setCompiler($instance);
        $this->assertEquals(Snooper::instance()->getCompiler(),$instance);
        Snooper::instance()->reset();
    }

    public function testSetProcessor()
    {
        Snooper::instance()->reset();
        $instance = new class extends Processor implements IProcessor {};
        Snooper::instance()->setProcessor($instance);
        $this->assertEquals(Snooper::instance()->getProcessor(),$instance);
        Snooper::instance()->reset();
    }

    public function testSetRenderer()
    {
        Snooper::instance()->reset();
        $instance = new class extends Renderer implements IRenderer {};
        Snooper::instance()->setRenderer($instance);
        $this->assertEquals(Snooper::instance()->getRenderer(),$instance);
        Snooper::instance()->reset();
    }

    public function testSetSerializer()
    {
        Snooper::instance()->reset();
        $instance = new class extends Serializer implements ISerializer {public function serialize($subject){}};
        Snooper::instance()->setSerializer($instance);
        $this->assertEquals(Snooper::instance()->getSerializer(),$instance);
        Snooper::instance()->reset();
    }

    public function testDeliverResponses()
    {
        Snooper::instance()->reset();

        $response = \Snooper\Components\Response\Container::instance();
        $response->add(\Snooper\Components\Response\Standard\Response::create(Deliver::create('head'),'console.log'));
        $response->add(\Snooper\Components\Response\Standard\Response::create(Deliver::create('head'),'console.log'));
        $response->add(\Snooper\Components\Response\Standard\Response::create(Deliver::create('head'),'console.log'));

        $response->add(\Snooper\Components\Response\Interaction\Response::create(FreeSelector::create('test'),'console.log'));
        $response->add(\Snooper\Components\Response\Interaction\Response::create(FreeSelector::create('test'),'console.log'));
        $response->add(\Snooper\Components\Response\Interaction\Response::create(FreeSelector::create('test'),'console.log'));
        $response->add(\Snooper\Components\Response\Interaction\Response::create(FreeSelector::create('test'),'console.log'));

        $this->assertEquals(
            Snooper::instance()->getDeliverResponses(),
            [
                0 => ['type'=>'standard','method'=>'console.log','params'=>null],
                1 => ['type'=>'standard','method'=>'console.log','params'=>null],
                2 => ['type'=>'standard','method'=>'console.log','params'=>null],
                3 => ['type'=>'interaction','source'=>null,'target'=>'test','event'=>'click','method'=>'console.log','params'=>null],
                4 => ['type'=>'interaction','source'=>null,'target'=>'test','event'=>'click','method'=>'console.log','params'=>null],
                5 => ['type'=>'interaction','source'=>null,'target'=>'test','event'=>'click','method'=>'console.log','params'=>null],
                6 => ['type'=>'interaction','source'=>null,'target'=>'test','event'=>'click','method'=>'console.log','params'=>null],
            ]
        );

        Snooper::instance()->reset();
    }

    public function testGetUndeliveredResponses()
    {
        Snooper::instance()->reset();

        $standardContainer = \Snooper\Components\Response\Container::instance();
        $standardContainer->add(\Snooper\Components\Response\Standard\Response::create(Deliver::create('head'),'console.log'));
        $standardContainer->add(\Snooper\Components\Response\Standard\Response::create(Deliver::create('head'),'console.log'));

        $this->assertEquals(2,Snooper::instance()->getUndeliveredResponseCount());

        Snooper::instance()->reset();
    }
}
