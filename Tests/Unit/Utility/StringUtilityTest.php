<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Utility;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Utility\StringUtility;

/**
 * Class StringUtilityTest
 * @package Snooper\Components\Tests\Unit\Utility
 */
class StringUtilityTest extends TestCase
{
    public function testStartsWith()
    {
        $this->assertTrue(StringUtility::startsWith('testMe12345','test'));
        $this->assertNotTrue(StringUtility::startsWith('tes','test'));
    }
    public function testEndsWith()
    {
        $this->assertTrue(StringUtility::endsWith('testMe12345foo','foo'));
        $this->assertNotTrue(StringUtility::endsWith('est','test'));
        $this->assertTrue(StringUtility::endsWith('test',''));
    }
}
