<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Utility\Traits\Container;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Utility\Traits\Container\Container;
use Snooper\Components\Utility\Traits\Container\ContainerFlat;
use Snooper\Components\Utility\Traits\Container\ContainerGrouped;
use Snooper\Components\Utility\Traits\Container\Sortable;
use Snooper\Components\Utility\Traits\Container\SortFlat;
use Snooper\Components\Utility\Traits\Container\SortGroup;

/**
 * Class ContainerTest
 * @package Snooper\Components\Tests\Unit\Utility\Traits\Container
 */
class ContainerTest extends TestCase
{
    public function testContainerWithInitialState()
    {
        $instance = new class
        {
            use Container;
        };

        $this->assertEquals([], $instance->getRaw());
        $this->assertEquals(0, $instance->count());
        $this->assertEquals(null, $instance->clear());
    }

    public function testContainerWithFilledState()
    {
        $instance = new class
        {
            use Container;

            public function __init()
            {
                $this->container = ['test1', 'test2'];
                $this->count = 2;
            }
        };

        $instance->__init();

        $this->assertEquals(['test1', 'test2'], $instance->getRaw());
        $this->assertEquals(2, $instance->count());
        $this->assertEquals(null, $instance->clear());
        $this->assertEquals([], $instance->getRaw());
        $this->assertEquals(0, $instance->count());
    }

    public function testContainerFlat()
    {
        $instance = new class
        {
            use Container;
            use ContainerFlat;
        };

        $this->assertEquals([], $instance->getRaw());
        $this->assertEquals(0, $instance->count());
        $instance->add('first');
        $this->assertEquals(['first'], $instance->getRaw());
        $this->assertEquals(1, $instance->count());
        $instance->add('second');
        $this->assertEquals(['first', 'second'], $instance->getRaw());
        $this->assertEquals(2, $instance->count());
        $instance->clear();
        $this->assertEquals([], $instance->getRaw());
        $this->assertEquals(0, $instance->count());
    }

    public function testContainerFlatSort()
    {
        $instance = new class
        {
            use Container;
            use ContainerFlat;
            use Sortable;
            use SortFlat;
        };

        $instance->add('first');
        $instance->add('second');

        $this->assertEquals([0=>'first', 1=>'second'], $instance->getRaw());

        $instance->setSortModeDesc();
        $instance->sort();
        $this->assertEquals([1=>'second',0=>'first'], $instance->getRaw());

        $instance->setSortModeAsc();
        $instance->sort();
        $this->assertEquals([0=>'first', 1=>'second'], $instance->getRaw());

        $instance->setSortModeDesc();
        $instance->sort();
        $this->assertEquals([1=>'second',0=>'first'], $instance->getRaw());
    }

    public function testContainerGrouped()
    {
        $instance = new class
        {
            use Container;
            use ContainerGrouped;
        };

        $this->assertEquals([], $instance->getRaw());
        $this->assertEquals(0, $instance->count());
        $instance->add('group1', 'test1');
        $this->assertEquals(['group1' => ['test1']], $instance->getRaw());
        $this->assertEquals(1, $instance->count());
        $instance->add('group1', 'test2');
        $this->assertEquals(['group1' => ['test1', 'test2']], $instance->getRaw());
        $this->assertEquals(1, $instance->count());
        $instance->add('group2', 'test1');
        $this->assertEquals(['group1' => ['test1', 'test2'], 'group2' => ['test1']], $instance->getRaw());
        $this->assertEquals(2, $instance->count());
        $instance->add('group2', 'test2');
        $this->assertEquals(['group1' => ['test1', 'test2'], 'group2' => ['test1', 'test2']], $instance->getRaw());
        $this->assertEquals(2, $instance->count());
        $instance->clear();
        $this->assertEquals([], $instance->getRaw());
        $this->assertEquals(0, $instance->count());
    }

    public function testContainerGroupedSortFlat()
    {
        $instance = new class
        {
            use Container;
            use ContainerGrouped;
            use Sortable;
            use SortGroup;
        };

        $instance->add('group3', 'test1');
        $instance->add('group1', 'test1');
        $instance->add('group2', 'test1');

        $instance->setSortModeAsc();
        $instance->sortFlat();
        $this->assertEquals(['group1'=>[0=>'test1'],'group2'=>[0=>'test1'],'group3'=>[0=>'test1']], $instance->getRaw());

        $instance->setSortModeDesc();
        $instance->sortFlat();
        $this->assertEquals(['group3'=>[0=>'test1'],'group2'=>[0=>'test1'],'group1'=>[0=>'test1']], $instance->getRaw());

    }

    public function testContainerGroupedSortDeep()
    {
        $instance = new class
        {
            use Container;
            use ContainerGrouped;
            use Sortable;
            use SortGroup;
        };

        $instance->add('group1','first1');
        $instance->add('group1','second1');

        $instance->add('group2','first2');
        $instance->add('group2','second2');


        $this->assertEquals(['group1'=>[0=>'first1',1=>'second1'],'group2'=>[0=>'first2',1=>'second2']], $instance->getRaw());

        $instance->setSortModeDesc();
        $instance->sortDeep();
        $this->assertEquals(['group1'=>[1=>'second1',0=>'first1'],'group2'=>[1=>'second2',0=>'first2']], $instance->getRaw());

        $instance->setSortModeAsc();
        $instance->sortDeep();
        $this->assertEquals(['group1'=>[0=>'first1',1=>'second1'],'group2'=>[0=>'first2',1=>'second2']], $instance->getRaw());

        $instance->setSortModeDesc();
        $instance->sortDeep();
        $this->assertEquals(['group1'=>[1=>'second1',0=>'first1'],'group2'=>[1=>'second2',0=>'first2']], $instance->getRaw());
    }

    public function testContainerGroupedGetByGroup()
    {
        $instance = new class
        {
            use Container;
            use ContainerGrouped;
        };

        $instance->add('group1', 'test1');
        $instance->add('group1', 'test2');
        $instance->add('group2', 'test1');
        $this->assertEquals($instance->getByGroup('group1'),['test1','test2']);
    }
}
