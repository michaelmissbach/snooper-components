<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Utility\Traits;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Tests\Fixtures\Utility\Traits\SingletonFixture;

/**
 * Class SingletonTest
 * @package Snooper\Components\Tests\Unit\Utility\Traits
 */
class SingletonTest extends TestCase
{
    public function testSingleton()
    {
        $instance = SingletonFixture::instance();
        $this->assertEquals($instance instanceof SingletonFixture,true);
    }
}
