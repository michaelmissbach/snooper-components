<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Utility\Traits;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Utility\Traits\ConstantFinderTrait;

/**
 * Class ConstantFinderTraitTest
 * @package Snooper\Components\Tests\Unit\Utility\Traits
 */
class ConstantFinderTraitTest extends TestCase
{
    public function testFindVariables()
    {
        $instance = new class {
            use ConstantFinderTrait;

            protected $__constants;

            const test = "12345";

            public function testIsValueInConstants($search)
            {
                return $this->isValueInConstants($search);
            }
        };

        $this->assertTrue($instance->testIsValueInConstants('12345'));
        $this->assertNotTrue($instance->testIsValueInConstants('notfound'));
    }

    public function testFindExistingVariables()
    {
        $instance = new class {
            use ConstantFinderTrait;

            const TEST1 = 'test1';

            protected $__constants = ['TEST1'=>'test1'];

            public function testIsValueInConstants($search)
            {
                return $this->isValueInConstants($search);
            }
        };

        $this->assertTrue($instance->testIsValueInConstants('test1'));
        $this->assertNotTrue($instance->testIsValueInConstants('notfound'));
    }

    public function testGetVariableNames()
    {
        $instance = new class {
            use ConstantFinderTrait;

            protected $__constants;

            const test1 = "12345";
            const test2 = "67890";

            public function testGetConstantNames($maxCount = null)
            {
                return $this->getConstantNames($maxCount);
            }
        };

        $this->assertEquals($instance->testGetConstantNames(),'"test1","test2"');
    }

    public function testGetExisitngVariableNames()
    {
        $instance = new class {
            use ConstantFinderTrait;

            protected $__constants = ['TEST1'=>'test1'];

            const TEST1 = "12345";

            public function testGetConstantNames($maxCount = null)
            {
                return $this->getConstantNames($maxCount);
            }
        };

        $this->assertEquals($instance->testGetConstantNames(),'"TEST1"');
    }
}
