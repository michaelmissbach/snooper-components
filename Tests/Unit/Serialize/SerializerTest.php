<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Serialize;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Serialize\Serializer;

/**
 * Class SerializerTest
 * @package Snooper\Components\Tests\Unit\Serialize
 */
class SerializerTest extends TestCase
{
    public function testSerialize()
    {
        $serializer = new Serializer();
        $input = new \stdClass();
        $inner = new \stdClass();
        $inner->test = '12345';
        $input->test = $inner;
        $result = $serializer->serialize($input);

        $this->assertEquals('{"test":{"test":"12345"}}',$result);
    }
}
