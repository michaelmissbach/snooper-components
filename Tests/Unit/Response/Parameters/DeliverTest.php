<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Response\Parameters;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Response\Parameters\Deliver;

/**
 * Class DeliverTest
 * @package Snooper\Components\Tests\Unit\Response\Parameters
 */
class DeliverTest extends TestCase
{
    public function testSimple()
    {
        $instance = Deliver::create('head',Deliver::PRE_SECTION);
        $this->assertEquals($instance->getSection(),'head');
        $this->assertEquals($instance->getDeliverTimeOfExecution(),Deliver::PRE_SECTION);
    }

    public function testSimpleWithError()
    {
        $instance = null;
        try {
            $instance = Deliver::create('head','unknown');
        } catch (\Exception $e) {
        }
        $this->assertNotTrue($instance instanceof Deliver);
    }
}
