<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Response\Parameters;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Response\Parameters\Parameter;

/**
 * Class ParameterTest
 * @package Snooper\Components\Tests\Unit\Response\Parameters
 */
class ParameterTest extends TestCase
{
    public function testWithoutParams()
    {
        $instance = null;
        try {
            $instance = Parameter::createFromArray(null);
        } catch (\Exception $e) {
        }
        $this->assertNotTrue($instance instanceof Parameter);
    }

    public function testWithWrongParameters()
    {
        $instance = null;
        try {
            $instance = Parameter::createFromArray([12,34]);
        } catch (\Exception $e) {
        }
        $this->assertNotTrue($instance instanceof Parameter);
    }

    public function testWithWrongParameterCount()
    {
        $instance = null;
        try {
            $instance = Parameter::createFromArray([12,true,34]);
        } catch (\Exception $e) {
        }
        $this->assertNotTrue($instance instanceof Parameter);
    }

    public function testSimpleOneArgument()
    {
        $instance = Parameter::createFromArray([12]);
        $this->assertEquals($instance->getArguments(),[0=>['param'=>12,'isRawEvalNeededParam'=>false]]);
    }

    public function testSimpleTwoArguments()
    {
        $instance = Parameter::createFromArray([12,true,34,false]);
        $this->assertEquals(
            $instance->getArguments(),
            [
                0=>['param'=>12,'isRawEvalNeededParam'=>true],
                1=>['param'=>34,'isRawEvalNeededParam'=>false]
            ]
        );
    }

    public function testSimpleCreateFromList()
    {
        $instance = Parameter::createFromList(12,true,34,false);
        $this->assertEquals(
            $instance->getArguments(),
            [
                0=>['param'=>12,'isRawEvalNeededParam'=>true],
                1=>['param'=>34,'isRawEvalNeededParam'=>false]
            ]
        );
    }

    public function testSimpleCreateFromArray()
    {
        $instance = Parameter::createFromArray([12,true,34,false]);
        $this->assertEquals(
            $instance->getArguments(),
            [
                0=>['param'=>12,'isRawEvalNeededParam'=>true],
                1=>['param'=>34,'isRawEvalNeededParam'=>false]
            ]
        );
    }
}
