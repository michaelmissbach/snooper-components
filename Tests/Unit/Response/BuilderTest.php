<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Response;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Response\B;
use Snooper\Components\Response\Builder;
use Snooper\Components\Response\Interaction\FreeSelector;
use Snooper\Components\Response\Interaction\Options\Delay;
use Snooper\Components\Response\Interaction\Options\SendFrontendTarget;
use Snooper\Components\Response\Interaction\SnooperTarget;
use Snooper\Components\Response\Parameters\Deliver;
use Snooper\Components\Response\Parameters\Parameter;

/**
 * Class BuilderTest
 * @package Snooper\Components\Tests\Unit\Response
 */
class BuilderTest extends TestCase
{
    public function testCreateStandardResponse()
    {
        $this->assertTrue(Builder::createStandardResponse(Builder::createDeliver(Deliver::PRE_SECTION),'console.log') instanceof \Snooper\Components\Response\Standard\Response);
        $this->assertTrue(B::createStandardResponse(Builder::createDeliver(Deliver::PRE_SECTION),'console.log') instanceof \Snooper\Components\Response\Standard\Response);
    }

    public function testCsr()
    {
        $this->assertTrue(B::csr(B::cd(Deliver::PRE_SECTION),'console.log') instanceof \Snooper\Components\Response\Standard\Response);
    }

    public function testCreateDeliver()
    {
        $this->assertTrue(Builder::createDeliver('head') instanceof Deliver);
        $this->assertTrue(B::createDeliver('head') instanceof Deliver);
    }

    public function testCd()
    {
        $this->assertTrue(B::cd('head') instanceof Deliver);
    }

    public function testCreateInteractionResponse()
    {
        $this->assertTrue(Builder::createInteractionResponse(Builder::createInteractionFreeSelector('test'),'console.log') instanceof \Snooper\Components\Response\Interaction\Response);
        $this->assertTrue(B::createInteractionResponse(B::createInteractionFreeSelector('test'),'console.log') instanceof \Snooper\Components\Response\Interaction\Response);
    }

    public function testCir()
    {
        $this->assertTrue(B::cir(Builder::createInteractionFreeSelector('test'),'console.log') instanceof \Snooper\Components\Response\Interaction\Response);
    }

    public function testCreateInteractionSnooperTarget()
    {
        $this->assertTrue(Builder::createInteractionSnooperTarget('.test') instanceof SnooperTarget);
        $this->assertTrue(B::createInteractionSnooperTarget('.test') instanceof SnooperTarget);
    }

    public function testCist()
    {
        $this->assertTrue(B::cist('.test') instanceof SnooperTarget);
    }

    public function testCreateInteractionFreeSelector()
    {
        $this->assertTrue(Builder::createInteractionFreeSelector('.test') instanceof FreeSelector);
        $this->assertTrue(B::createInteractionFreeSelector('.test') instanceof FreeSelector);
    }

    public function testCifs()
    {
        $this->assertTrue(B::cifs('.test') instanceof FreeSelector);
    }

    public function testCreateInteractionOptionDelay()
    {
        $this->assertTrue(Builder::createInteractionOptionDelay(1000) instanceof Delay);
        $this->assertTrue(B::createInteractionOptionDelay(1000) instanceof Delay);
    }

    public function testCiod()
    {
        $this->assertTrue(B::ciod(1000) instanceof Delay);
    }

    public function testCreateInteractionOptionSendFrontendTarget()
    {
        $this->assertTrue(Builder::createInteractionOptionSendFrontendTarget() instanceof SendFrontendTarget);
        $this->assertTrue(B::createInteractionOptionSendFrontendTarget() instanceof SendFrontendTarget);
    }

    public function testCiosft()
    {
        $this->assertTrue(B::ciosft() instanceof SendFrontendTarget);
    }

    public function testCreateParameter()
    {
        $this->assertTrue(Builder::createParameter(12) instanceof Parameter);
        $this->assertTrue(B::createParameter(12) instanceof Parameter);
    }

    public function testCp()
    {
        $this->assertTrue(B::cp(12) instanceof Parameter);
    }
}
