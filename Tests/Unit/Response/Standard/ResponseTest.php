<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Response\Standard;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Response\Parameters\Deliver;
use Snooper\Components\Response\Parameters\Parameter;
use Snooper\Components\Response\Standard\Response;

/**
 * Class ResponseTest
 * @package Snooper\Components\Tests\Unit\Response\Standard
 */
class ResponseTest extends TestCase
{
    public function testSimple()
    {
        $deliver = Deliver::create('head',Deliver::PRE_SECTION);
        $response = Response::create($deliver,'console.log');
        $this->assertEquals($response->getJavascriptMethodName(),'console.log');
        $this->assertEquals($response->getJavascriptParameter(),null);
    }

    public function testWithParameters()
    {
        $deliver = Deliver::create('head',Deliver::PRE_SECTION);
        $params = Parameter::createFromList('test');
        $response = Response::create($deliver,'console.log',$params);
        $this->assertEquals($response->getJavascriptMethodName(),'console.log');
        $this->assertEquals($response->getJavascriptParameter(),$params);
    }

    public function testWithWrongParameters()
    {
        $deliver = Deliver::create('head',Deliver::PRE_SECTION);
        $response = Response::create($deliver,'console.log',['test']);
        $this->assertEquals($response->getJavascriptMethodName(),'Snooper.log');
    }
}
