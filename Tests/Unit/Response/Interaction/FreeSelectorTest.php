<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Response\Interaction;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Response\Interaction\FreeSelector;
use Snooper\Components\Response\Interaction\Interaction;

/**
 * Class FreeSelectorTest
 * @package Snooper\Components\Tests\Unit\Response\Interaction
 */
class FreeSelectorTest extends TestCase
{
    public function testSimple1()
    {
        $instance = FreeSelector::create('test-target',Interaction::ON_ABORT);
        $this->assertEquals($instance->getTarget(),'test-target');
        $this->assertEquals($instance->getEvent(),Interaction::ON_ABORT);
    }
    public function testSimpleWithError()
    {
        $instance=null;
        try {
            $instance = FreeSelector::create('test-target','unknown-event');
        } catch (\Exception $e) {
        }
        $this->assertNotTrue($instance instanceof FreeSelector);
    }
}
