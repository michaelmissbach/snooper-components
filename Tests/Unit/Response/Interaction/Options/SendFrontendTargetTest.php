<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Response\Interaction\Options;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Response\Interaction\Options\SendFrontendTarget;

/**
 * Class SendFrontendTargetTest
 * @package Snooper\Components\Tests\Unit\Response\Interaction\Options
 */
class SendFrontendTargetTest extends TestCase
{
    public function testSimple()
    {
        $instance = SendFrontendTarget::create(SendFrontendTarget::SEND_FE_TARGET_NONE);
        $this->assertEquals($instance->getJsConfigKey(),'sendFeTargetPostion');
        $this->assertEquals($instance->getConfig(),SendFrontendTarget::SEND_FE_TARGET_NONE);
    }

    public function testSimpleWithError()
    {
        $instance = null;
        try {
            $instance = SendFrontendTarget::create('something');
        } catch (\Exception $e) {
        }
        $this->assertNotTrue($instance instanceof SendFrontendTarget);
    }
}
