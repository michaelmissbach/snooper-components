<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Response\Interaction\Options;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Response\Interaction\Options\Remove;

/**
 * Class RemoveTest
 * @package Snooper\Components\Tests\Unit\Response\Interaction\Options
 */
class RemoveTest extends TestCase
{
    public function testSimple()
    {
        $instance = Remove::create();
        $this->assertEquals($instance->getJsConfigKey(),'remove');
        $this->assertEquals($instance->getConfig(),true);
    }
}
