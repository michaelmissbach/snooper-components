<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Response\Interaction\Options;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Response\Interaction\Options\Delay;

/**
 * Class DelayTest
 * @package Snooper\Components\Tests\Unit\Response\Interaction\Options
 */
class DelayTest extends TestCase
{
    public function testSimple()
    {
        $instance = Delay::create(111);
        $this->assertEquals($instance->getJsConfigKey(),'delay');
        $this->assertEquals($instance->getConfig(),111);
    }
}
