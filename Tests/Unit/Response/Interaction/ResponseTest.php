<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Response\Interaction;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Response\Interaction\Interaction;
use Snooper\Components\Response\Interaction\Options\Delay;
use Snooper\Components\Response\Interaction\Options\IInteractionOption;
use Snooper\Components\Response\Interaction\Response;
use Snooper\Components\Response\Parameters\Parameter;

/**
 * Class ResponseTest
 * @package Snooper\Components\Tests\Unit\Response\Interaction
 */
class ResponseTest extends TestCase
{
    public function testSimple()
    {
        $interaction = Interaction::create('target');

        $response = Response::create($interaction,'console.log');
        $this->assertEquals($response->getJavascriptMethodName(),'console.log');
        $this->assertEquals($response->getInteraction(),$interaction);
        $this->assertEquals($response->getOptions(),null);
        $this->assertEquals($response->getJavascriptParameter(),null);
    }

    public function testWithParameters()
    {
        $interaction = Interaction::create('target');
        $params = Parameter::createFromList('test');

        $response = Response::create($interaction,'console.log',$params);
        $this->assertEquals($response->getJavascriptMethodName(),'console.log');
        $this->assertEquals($response->getInteraction(),$interaction);
        $this->assertEquals($response->getOptions(),null);
        $this->assertEquals($response->getJavascriptParameter(),$params);
    }

    public function testWithParametersAndOptions()
    {
        $interaction = Interaction::create('target');
        $interactionOption1 = Delay::create('target');
        $interactionOption2 = Delay::create('target');
        $params = Parameter::createFromList('test');

        $response = Response::create($interaction,'console.log',$params,[$interactionOption1,$interactionOption2]);
        $this->assertEquals($response->getJavascriptMethodName(),'console.log');
        $this->assertEquals($response->getInteraction(),$interaction);
        $this->assertEquals($response->getOptions(),[$interactionOption1,$interactionOption2]);
        $this->assertEquals($response->getJavascriptParameter(),$params);
    }

    public function testWithWrongParameters()
    {
        $interaction = Interaction::create('target');

        $response = Response::create($interaction,'console.log',['test']);
        $this->assertEquals($response->getJavascriptMethodName(),'Snooper.log');
        $this->assertEquals($response->getInteraction(),$interaction);
        $this->assertEquals($response->getOptions(),null);
    }

    public function testWithParametersAndWrongOptions()
    {
        $interaction = Interaction::create('target');
        $interactionOption1 = Delay::create('target');
        $interactionOption2 = Delay::create('target');
        $params = Parameter::createFromList('test');

        $response = Response::create($interaction,'console.log',$params,[[],$interactionOption1,[],$interactionOption2]);
        $this->assertEquals($response->getJavascriptMethodName(),'console.log');
        $this->assertEquals($response->getInteraction(),$interaction);
        $this->assertEquals($response->getOptions(),[$interactionOption1,$interactionOption2]);
        $this->assertEquals($response->getJavascriptParameter(),$params);
    }
}
