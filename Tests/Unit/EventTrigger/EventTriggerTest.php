<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\EventTrigger;

use PHPUnit\Framework\TestCase;
use Snooper\Components\EventTrigger\EventTrigger;

/**
 * Class EventTriggerTest
 * @package Snooper\Components\Tests\Unit\EventTrigger
 */
class EventTriggerTest extends TestCase
{
    public function testEventEmpty()
    {
        $event = EventTrigger::create('test_name');
        $this->assertEquals('test_name',$event->getEventName());
    }

    public function testEventUndockParameters()
    {
        $source = new \stdClass();
        $event = EventTrigger::create('test_name', ['object'=>$source]);
        $this->assertNotTrue($event->getParameters()->get('object') === $source);
    }

    public function testEventPropagationStopped()
    {
        $event = EventTrigger::create('test');
        $event->stopPropagation();
        $this->assertTrue($event->isPropagationStopped());
    }

    public function testEventSetGetContext()
    {
        $event = EventTrigger::create('test');
        $context = new \stdClass();
        $event->setContext($context);
        $this->assertEquals($event->getContext(),$context);
    }
}
