<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Config;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Config\Config;

/**
 * Class ConfigTest
 * @package Snooper\Components\Tests\Unit\Config
 */
class ConfigTest extends TestCase
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @param bool $forceNew
     * @return Config
     */
    protected function config($forceNew = false)
    {
        if (!$this->config || $forceNew) {
            $this->config = new class extends Config {

                protected $rawParameters = [
                    'first_section' => 'head',
                    'last_section' => 'footer',
                    'sections' => [
                        'head','body','footer'
                    ],
                    'json_response_key' => 'snooper_data',
                    'xhr_send_mode'=>self::XHR_SEND_METHOD_BOTH,
                    'debug' => [
                        'nested_level' => 25,
                        'url_param' => 'snooper_debug',
                        'session_param' => 'snooper_debug',
                        'debug_environments' => [
                            'dev'
                        ]
                    ],
                    'log' => [
                        'excludeClass' => [
                            'Doctrine\ORM\PersistentCollection'
                        ]
                    ],
                    'ignoredRoutes' => [
                        '/\/admin$/'
                    ]
                ];
            };
        }

        return $this->config;
    }

    public function testGetFirstSection()
    {
        $this->assertEquals('head',$this->config()->getFirstSection());
    }

    public function testGetLastSection()
    {
        $this->assertEquals('footer',$this->config()->getLastSection());
    }

    public function testGetAvailableSections()
    {
        $this->assertEquals(['head','body','footer'],$this->config()->getAvailableSections());
    }

    public function testGetDebugNestedLevel()
    {
        $this->assertEquals(25,$this->config()->getDebugNestedLevel());
    }

    public function testGetDebugUrlParam()
    {
        $this->assertEquals('snooper_debug',$this->config()->getDebugUrlParam());
    }

    public function testGetDebugSessionParam()
    {
        $this->assertEquals('snooper_debug',$this->config()->getDebugSessionParam());
    }

    public function testGetDebugEnvironments()
    {
        $this->assertEquals(['dev'],$this->config()->getDebugEnvironments());
    }

    public function testIsDebugMode()
    {
        $this->assertEquals(false,$this->config()->isDebugMode());
        $this->config->setDebugMode(true);
        $this->assertEquals(true,$this->config()->isDebugMode());
    }

    public function testGetLogExcludedClasses()
    {
        $this->assertEquals(['Doctrine\ORM\PersistentCollection'],$this->config()->getLogExcludedClasses());
    }

    public function testGetJsonResponseKey()
    {
        $this->assertEquals('snooper_data',$this->config()->getJsonResponseKey());
    }

    public function testGetIgnoredRoutes()
    {
        $this->assertEquals(['/\/admin$/'],$this->config()->getIgnoredRoutes());
    }

    public function testGetXhrSendMode()
    {
        $this->assertEquals(Config::XHR_SEND_METHOD_BOTH,$this->config()->getXhrSendMode());
    }

    public function testConfigMerge()
    {
        $this->config()->mergeConfig(['first_section'=>'test']);
        $this->assertEquals('test',$this->config()->getFirstSection());
        $this->config()->mergeConfig(['first_section'=>'head']);
        $this->assertEquals('head',$this->config()->getFirstSection());

        $this->config()->mergeConfig(['debug'=>['nested_level'=>11]]);
        $this->assertEquals(11,$this->config()->getDebugNestedLevel());
        $this->config()->mergeConfig(['debug'=>['nested_level'=>25]]);
        $this->assertEquals(25,$this->config()->getDebugNestedLevel());
    }
}
