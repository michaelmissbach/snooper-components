<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Log;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Log\ILog;
use Snooper\Components\Log\Log;
use Snooper\Components\Log\LoggerContainer;

/**
 * Class LoggerContainerTest
 * @package Snooper\Components\Tests\Unit\Log
 */
class LoggerContainerTest extends TestCase
{
    public function testContainerAddNotEnabled()
    {
        LoggerContainer::instance()->clear();
        LoggerContainer::instance()->add('group1','test');

        $this->assertEquals(0,LoggerContainer::instance()->getLogCount());
        $this->assertEquals(0,LoggerContainer::instance()->count());
        $this->assertEquals([],LoggerContainer::instance()->getRaw());

        LoggerContainer::instance()->clear();
    }

    public function testContainerAdd()
    {
        LoggerContainer::instance()->clear();
        LoggerContainer::instance()->setEnabled(true);
        LoggerContainer::instance()->add('group1','test');

        $this->assertEquals(1,LoggerContainer::instance()->getLogCount());
        $this->assertEquals(1,LoggerContainer::instance()->count());

        LoggerContainer::instance()->clear();
    }

    public function testContainerAddMutliple()
    {
        LoggerContainer::instance()->clear();
        LoggerContainer::instance()->setEnabled(true);
        LoggerContainer::instance()->add('group1','test');
        LoggerContainer::instance()->add('group1','test');
        LoggerContainer::instance()->add('group2','test');

        $this->assertEquals(3,LoggerContainer::instance()->getLogCount());
        $this->assertEquals(2,LoggerContainer::instance()->count());

        LoggerContainer::instance()->clear();
    }

    public function testContainerClear()
    {
        LoggerContainer::instance()->clear();
        LoggerContainer::instance()->setEnabled(true);
        LoggerContainer::instance()->add('group1','test');
        LoggerContainer::instance()->add('group1','test');
        LoggerContainer::instance()->add('group2','test');

        LoggerContainer::instance()->clear();
        $this->assertEquals(0,LoggerContainer::instance()->getLogCount());
        $this->assertEquals(0,LoggerContainer::instance()->count());
        $this->assertEquals([],LoggerContainer::instance()->getRaw());

        LoggerContainer::instance()->clear();
    }

    public function testGetSetEnabled()
    {
        LoggerContainer::instance()->clear();
        LoggerContainer::instance()->setEnabled(true);
        $this->assertTrue(LoggerContainer::instance()->isEnabled());

        LoggerContainer::instance()->clear();
    }

    public function testGetSetLogCount()
    {
        LoggerContainer::instance()->clear();
        LoggerContainer::instance()->setLogCount(12);
        $this->assertEquals(12,LoggerContainer::instance()->getLogCount());

        LoggerContainer::instance()->clear();
    }
}
