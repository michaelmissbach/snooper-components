<?php

namespace Snooper\Components\Tests\Unit\Log;

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use PHPUnit\Framework\TestCase;
use Snooper\Components\Log\Log;

/**
 * Class LogTest
 * @package Snooper\Components\Tests\Unit\Log
 */
class LogTest extends TestCase
{
    public function testSimple()
    {
        $log = new Log('message',1,'type');

        $this->assertEquals($log->getMessage(),'message');
        $this->assertEquals($log->getExecutionTime(),1000000);
        $this->assertEquals($log->getType(),'type');
    }
}
