<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Processor;

use PHPUnit\Framework\TestCase;
use Snooper\Components\EventTrigger\EventTrigger;
use Snooper\Components\Provider\ProviderCallContainer;
use Snooper\Components\Provider\ProviderCallDefinition;
use Snooper\Components\Snooper;
use Snooper\Components\Tests\Fixtures\Provider\Provider;

/**
 * Class ProcessorTest
 * @package Snooper\Components\Tests\Unit\Processor
 */
class ProcessorTest extends TestCase
{
    public function testPrepareProviderCallsByPrioStandardResponse()
    {
        Snooper::instance()->reset();
        Snooper::instance()->addProvider(new Provider());
        $e = EventTrigger::create('eventStandardResponse');
        Snooper::instance()->getController()->onRaiseEvent($e);

        $result = ProviderCallContainer::instance()->getRaw();

        $this->assertEquals($result[ProviderCallDefinition::PRIORIZATION_LOWEST - 1][0]->getMethod(),'preRaiseEvent');
        $this->assertEquals($result[0][0]->getMethod(),'m1');
        $this->assertEquals($result[ProviderCallDefinition::PRIORIZATION_HIGHEST + 1][0]->getMethod(),'postRaiseEvent');
        Snooper::instance()->reset();
    }

    public function testPrepareProviderCallsByPrioInteractionResponse()
    {
        Snooper::instance()->reset();
        Snooper::instance()->addProvider(new Provider());
        $e = EventTrigger::create('eventInteractionResponse');
        Snooper::instance()->getController()->onRaiseEvent($e);

        $result = ProviderCallContainer::instance()->getRaw();

        $this->assertEquals($result[ProviderCallDefinition::PRIORIZATION_LOWEST - 1][0]->getMethod(),'preRaiseEvent');
        $this->assertEquals($result[0][0]->getMethod(),'m2');
        $this->assertEquals($result[ProviderCallDefinition::PRIORIZATION_HIGHEST + 1][0]->getMethod(),'postRaiseEvent');
        Snooper::instance()->reset();
    }

    public function testPrepareProviderCallsByPrioBothResponses()
    {
        Snooper::instance()->reset();
        Snooper::instance()->addProvider(new Provider());
        $e = EventTrigger::create('eventMixed');
        Snooper::instance()->getController()->onRaiseEvent($e);

        $result = ProviderCallContainer::instance()->getRaw();

        $this->assertEquals($result[ProviderCallDefinition::PRIORIZATION_LOWEST - 1][0]->getMethod(),'preRaiseEvent');
        $this->assertEquals($result[0][0]->getMethod(),'m3');
        $this->assertEquals($result[ProviderCallDefinition::PRIORIZATION_HIGHEST + 1][0]->getMethod(),'postRaiseEvent');
        Snooper::instance()->reset();
    }

    public function testExecutesProviderCallsStandardResponse()
    {
        Snooper::instance()->reset();
        Snooper::instance()->addProvider(new Provider());
        $e = EventTrigger::create('eventStandardResponse');
        Snooper::instance()->getController()->onRaiseEvent($e);
        Snooper::instance()->getController()->onRaiseEventExecution($e);

        $resultStandard = \Snooper\Components\Response\Container::instance()->getRaw();
        $this->assertEquals($resultStandard[0]->getJavascriptMethodName(),'console.log');
        $this->assertEquals(count($resultStandard),1);

        Snooper::instance()->reset();
    }

    public function testExecutesProviderCallsInteractionResponse()
    {
        Snooper::instance()->reset();
        Snooper::instance()->addProvider(new Provider());
        $e = EventTrigger::create('eventInteractionResponse');
        Snooper::instance()->getController()->onRaiseEvent($e);
        Snooper::instance()->getController()->onRaiseEventExecution($e);

        $resultInteraction = \Snooper\Components\Response\Container::instance()->getRaw();
        $this->assertEquals($resultInteraction[0]->getJavascriptMethodName(),'console.log');
        $this->assertEquals(count($resultInteraction),1);
        Snooper::instance()->reset();
    }

    public function testExecutesProviderCallsBothResponse()
    {
        Snooper::instance()->reset();
        Snooper::instance()->addProvider(new Provider());
        $e = EventTrigger::create('eventMixed');
        Snooper::instance()->getController()->onRaiseEvent($e);
        Snooper::instance()->getController()->onRaiseEventExecution($e);

        $result = \Snooper\Components\Response\Container::instance()->getRaw();
        $this->assertEquals($result[0]->getJavascriptMethodName(),'console.log');
        $this->assertEquals($result[1]->getJavascriptMethodName(),'console.log');
        $this->assertEquals(count($result),2);
        Snooper::instance()->reset();
    }
}
