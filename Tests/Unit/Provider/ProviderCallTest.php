<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Provider;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Provider\ProviderCall;
use Snooper\Components\Tests\Fixtures\Provider\Provider;

/**
 * Class ProviderCallTest
 * @package Snooper\Components\Tests\Unit\Provider
 */
class ProviderCallTest extends TestCase
{
    public function testCreateEmpty()
    {
        $provider = new Provider();
        $pc = ProviderCall::create($provider,'m1');

        $this->assertEquals($pc->getProvider(),$provider);
        $this->assertEquals($pc->getMethod(),'m1');
        $this->assertEquals($pc->getAdditionalParameters(),null);

    }

    public function testCreateWithArguments()
    {
        $provider = new Provider();
        $pc = ProviderCall::create($provider,'m1',['test1','test2']);

        $this->assertEquals($pc->getProvider(),$provider);
        $this->assertEquals($pc->getMethod(),'m1');
        $this->assertEquals($pc->getAdditionalParameters(),['test1','test2']);
    }
}
