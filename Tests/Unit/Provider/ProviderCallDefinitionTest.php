<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Provider;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Provider\ProviderCallDefinition;

/**
 * Class ProviderCallDefinitionTest
 * @package Snooper\Components\Tests\Unit\Provider
 */
class ProviderCallDefinitionTest extends TestCase
{
    public function testCreateEmpty()
    {
        $pcd = ProviderCallDefinition::create('m1');

        $this->assertEquals($pcd->getMethod(),'m1');
        $this->assertEquals($pcd->getAdditionalParameters(),null);
        $this->assertEquals($pcd->getPriorization(),ProviderCallDefinition::PRIORIZATION_STANDARD);
    }

    public function testCreateWithArguments()
    {
        $pcd = ProviderCallDefinition::create('m1',['test1','test2']);

        $this->assertEquals($pcd->getMethod(),'m1');
        $this->assertEquals($pcd->getAdditionalParameters(),['test1','test2']);
        $this->assertEquals($pcd->getPriorization(),ProviderCallDefinition::PRIORIZATION_STANDARD);
    }

    public function testCreateWithArgumentsAndPrio()
    {
        $pcd = ProviderCallDefinition::create('m1',['test1','test2'],25);

        $this->assertEquals($pcd->getMethod(),'m1');
        $this->assertEquals($pcd->getAdditionalParameters(),['test1','test2']);
        $this->assertEquals($pcd->getPriorization(),25);
    }

    public function testSetter()
    {
        $pcd = ProviderCallDefinition::create('m1',['test1','test2'],25);

        $pcd->setMethod('m2');
        $pcd->setAdditionalParameters(['test3','test4']);
        $pcd->setPriorization(111);

        $this->assertEquals($pcd->getMethod(),'m2');
        $this->assertEquals($pcd->getAdditionalParameters(),['test3','test4']);
        $this->assertEquals($pcd->getPriorization(),111);
    }
}
