<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Provider;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Provider\IProviderCallDefinition;
use Snooper\Components\Provider\ProviderCall;
use Snooper\Components\Provider\ProviderCallContainer;
use Snooper\Components\Tests\Fixtures\Provider\Provider;

/**
 * Class ProviderCallContainerTest
 * @package Snooper\Components\Tests\Unit\Provider
 */
class ProviderCallContainerTest extends TestCase
{
    public function testAdd()
    {
        ProviderCallContainer::instance()->clear();

        $providerCall = ProviderCall::create(new Provider(),'m1',[],false);

        ProviderCallContainer::instance()->add($providerCall,10);
        ProviderCallContainer::instance()->add($providerCall,IProviderCallDefinition::PRIORIZATION_LOWEST - 100);
        ProviderCallContainer::instance()->add($providerCall,IProviderCallDefinition::PRIORIZATION_HIGHEST + 100);
        ProviderCallContainer::instance()->add($providerCall,IProviderCallDefinition::PRIORIZATION_HIGHEST + 100, true);

        ProviderCallContainer::instance()->setSortModeAsc();
        ProviderCallContainer::instance()->sortFlat();

        $this->assertEquals(
            ProviderCallContainer::instance()->getRaw(),
            [
                IProviderCallDefinition::PRIORIZATION_LOWEST => [0=>$providerCall],
                10 => [0=>$providerCall],
                IProviderCallDefinition::PRIORIZATION_HIGHEST => [0=>$providerCall],
                IProviderCallDefinition::PRIORIZATION_HIGHEST + 100 => [0=>$providerCall],
            ]
        );

        ProviderCallContainer::instance()->clear();
    }

    public function testDeepYielded()
    {
        ProviderCallContainer::instance()->clear();

        $providerCall = ProviderCall::create(new Provider(),'m1',[]);

        ProviderCallContainer::instance()->add($providerCall,10);


        foreach(ProviderCallContainer::instance()->getDeepYielded() as $result) {
            $this->assertEquals($result->getMethod(),'m1');
        }

        ProviderCallContainer::instance()->clear();
    }
}
