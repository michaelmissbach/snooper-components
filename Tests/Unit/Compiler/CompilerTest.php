<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Compiler;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Compiler\CompiledContainer;
use Snooper\Components\Config\Config;
use Snooper\Components\EventTrigger\EventTrigger;
use Snooper\Components\Response\Interaction\FreeSelector;
use Snooper\Components\Response\Parameters\Deliver;
use Snooper\Components\Snooper;
use Snooper\Components\Tests\Fixtures\Provider\Provider;
use Snooper\Components\Tests\Fixtures\Provider\ProviderPreAndPostEvents;
use Snooper\Components\Utility\StringUtility;

/**
 * Class CompilerTest
 * @package Snooper\Components\Tests\Unit\Compiler
 */
class CompilerTest extends TestCase
{
    protected function getConfig()
    {
        return new class extends Config {

            protected $rawParameters = [
                'first_section' => 'head',
                'last_section' => 'footer',
                'sections' => [
                    'head','body','footer'
                ],
                'json_response_key' => 'snooper_data',
                'xhr_send_mode'=>self::XHR_SEND_METHOD_BOTH,
                'debug' => [
                    'nested_level' => 25,
                    'url_param' => 'snooper_debug',
                    'session_param' => 'snooper_debug',
                    'debug_environments' => [
                        'dev'
                    ]
                ],
                'log' => [
                    'excludeClass' => [
                        'Doctrine\ORM\PersistentCollection'
                    ]
                ],
                'ignoredRoutes' => [
                    '/\/admin$/'
                ]
            ];
        };
    }

    public function testExecutesProviderCallsStandardResponse()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());
        Snooper::instance()->addProvider(new Provider());
        $e = EventTrigger::create('eventStandardResponse');
        Snooper::instance()->getController()->onRaiseEvent($e);
        Snooper::instance()->getController()->onRaiseEventExecution($e);
        Snooper::instance()->getController()->outputSection('head');
        Snooper::instance()->getController()->outputSection('body');
        Snooper::instance()->getController()->outputSection('footer');

        $result = CompiledContainer::instance()->getRaw();

        $this->assertTrue(array_key_exists('head',$result));
        $this->assertNotTrue(array_key_exists('body',$result));
        $this->assertNotTrue(array_key_exists('footer',$result));
        $head = $result['head'];
        $this->assertTrue(StringUtility::startsWith($head[0],'<script type="text/javascript">'));
        $this->assertEquals($head[1],'<script>console.log("hello world from head");</script>');
        $this->assertEquals($head[2],'<script type="text/javascript">Snooper.execute([{"type":"standard","method":"console.log","params":[{"param":"hello world from m1","isRawEvalNeededParam":false}]}]);</script>');

        Snooper::instance()->reset();
    }

    public function testExecutesProviderCallsInteractionResponse()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());
        Snooper::instance()->addProvider(new Provider());
        $e = EventTrigger::create('eventInteractionResponse');
        Snooper::instance()->getController()->onRaiseEvent($e);
        Snooper::instance()->getController()->onRaiseEventExecution($e);
        Snooper::instance()->getController()->outputSection('head');
        Snooper::instance()->getController()->outputSection('body');
        Snooper::instance()->getController()->outputSection('footer');

        $result = CompiledContainer::instance()->getRaw();

        $this->assertTrue(array_key_exists('head',$result));
        $this->assertNotTrue(array_key_exists('body',$result));
        $this->assertTrue(array_key_exists('footer',$result));
        $head = $result['head'];
        $footer = $result['footer'];
        $this->assertTrue(StringUtility::startsWith($head[0],'<script type="text/javascript">'));
        $this->assertEquals($head[1],'<script>console.log("hello world from head");</script>');
        $this->assertTrue(StringUtility::startsWith($footer[0],'<script type="text/javascript">Snooper.execute('));

        Snooper::instance()->reset();
    }

    public function testExecutesProviderCallsBothResponse()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());
        Snooper::instance()->addProvider(new Provider());
        $e = EventTrigger::create('eventMixed');
        Snooper::instance()->getController()->onRaiseEvent($e);
        Snooper::instance()->getController()->onRaiseEventExecution($e);
        Snooper::instance()->getController()->outputSection('head');
        Snooper::instance()->getController()->outputSection('body');
        Snooper::instance()->getController()->outputSection('footer');

        $result = CompiledContainer::instance()->getRaw();

        $this->assertTrue(array_key_exists('head',$result));
        $this->assertNotTrue(array_key_exists('body',$result));
        $this->assertTrue(array_key_exists('footer',$result));
        $head = $result['head'];
        $footer = $result['footer'];
        $this->assertTrue(StringUtility::startsWith($head[0],'<script type="text/javascript">'));
        $this->assertEquals($head[1],'<script>console.log("hello world from head");</script>');
        $this->assertEquals($head[2],'<script type="text/javascript">Snooper.execute([{"type":"standard","method":"console.log","params":[{"param":"hello world from m1","isRawEvalNeededParam":false}]}]);</script>');

        $this->assertTrue(StringUtility::startsWith($footer[0],'<script type="text/javascript">Snooper.execute('));

        Snooper::instance()->reset();
    }

    public function testPreAndPostRaiseEvents()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());
        Snooper::instance()->addProvider(new ProviderPreAndPostEvents());
        $e = EventTrigger::create('event');

        Snooper::instance()->getController()->onRaiseEvent($e);
        Snooper::instance()->getController()->onRaiseEventExecution($e);
        Snooper::instance()->getController()->outputSection('head');
        Snooper::instance()->getController()->outputSection('body');
        Snooper::instance()->getController()->outputSection('footer');

        $result = CompiledContainer::instance()->getRaw();

        $head = $result['head'];
        $this->assertTrue(StringUtility::startsWith($head[0],'<script type="text/javascript">'));
        $this->assertEquals($head[1],'<script type="text/javascript">Snooper.execute([{"type":"standard","method":"console.log","params":[{"param":"hello world from preRaiseEvent","isRawEvalNeededParam":false}]},{"type":"standard","method":"console.log","params":[{"param":"hello world from postRaiseEvent","isRawEvalNeededParam":false}]}]);</script>');

        Snooper::instance()->reset();
    }

    public function testAddPreCompileAdditionalTemplates()
    {
        Snooper::instance()->reset();
        Snooper::instance()->setConfig($this->getConfig());
        Snooper::instance()->addProvider(new Provider());
        $e = EventTrigger::create('some');

        Snooper::instance()->getCompiler()->addPreCompileAdditionalTemplates('ADDITIONAL-PRE1');
        Snooper::instance()->getCompiler()->addPreCompileAdditionalTemplates('ADDITIONAL-PRE2');
        Snooper::instance()->getCompiler()->addPostCompileAdditionalTemplates('ADDITIONAL-POST1');
        Snooper::instance()->getCompiler()->addPostCompileAdditionalTemplates('ADDITIONAL-POST2');

        Snooper::instance()->getController()->onRaiseEvent($e);
        Snooper::instance()->getController()->onRaiseEventExecution($e);
        Snooper::instance()->getController()->outputSection('head');
        Snooper::instance()->getController()->outputSection('body');
        Snooper::instance()->getController()->outputSection('footer');

        $result = CompiledContainer::instance()->getRaw();

        $head = $result['head'];
        $footer = $result['footer'];
        $this->assertTrue(StringUtility::startsWith($head[0],'<script type="text/javascript">'));
        $this->assertEquals($head[1],'ADDITIONAL-PRE1');
        $this->assertEquals($head[2],'ADDITIONAL-PRE2');
        $this->assertEquals($head[3],'<script>console.log("hello world from head");</script>');
        $this->assertEquals($footer[0],'ADDITIONAL-POST1');
        $this->assertEquals($footer[1],'ADDITIONAL-POST2');

        Snooper::instance()->reset();
    }
}
