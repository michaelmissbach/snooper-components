<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Tests\Unit\Compiler;

use PHPUnit\Framework\TestCase;
use Snooper\Components\Compiler\CompiledContainer;

/**
 * Class CompiledContainerTest
 * @package Snooper\Components\Tests\Unit\Compiler
 */
class CompiledContainerTest extends TestCase
{
    public function testGetCompiledSectionNames()
    {
        $instance = CompiledContainer::instance();

        $instance->add('group1', 'test1');
        $instance->add('group1', 'test2');
        $instance->add('group2', 'test1');

        $this->assertEquals(['group1','group2'],CompiledContainer::instance()->getCompiledSectionNames());

        CompiledContainer::instance()->clear();
    }
}
