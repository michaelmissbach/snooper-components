<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Compiler;

use Snooper\Components\Response\Interaction\IResponse;

/**
 * Interface ICompiler
 * @package Snooper\Components\Compiler
 */
interface ICompiler
{
    /**
     * @param $template
     */
    public function addPreCompileAdditionalTemplates($template);

    /**
     * @param $template
     */
    public function addPostCompileAdditionalTemplates($template);

    /**
     *
     */
    public function runSection($section);
}
