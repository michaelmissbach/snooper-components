<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Compiler;

use Snooper\Components\Utility\Traits\Container\Container;
use Snooper\Components\Utility\Traits\Container\ContainerGrouped;
use Snooper\Components\Utility\Traits\Singleton;

/**
 * Class CompiledContainer
 * @package Snooper\Components\Compiler
 */
class CompiledContainer
{
    use Singleton;
    use Container;
    use ContainerGrouped;
    /**
     * @return array
     */
    public function getCompiledSectionNames()
    {
        return array_keys($this->container);
    }
}
