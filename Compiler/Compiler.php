<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Compiler;

use Snooper\Components\Log\ILog;
use Snooper\Components\Log\LoggerContainer;
use Snooper\Components\Provider\IProvider;
use Snooper\Components\Provider\ProviderContainer;
use Snooper\Components\Response\ICommonResponse;
use Snooper\Components\Response\Interaction\Options\IInteractionOption;
use Snooper\Components\Response\Parameters\Deliver;
use Snooper\Components\Response\Parameters\Parameter;
use Snooper\Components\Snooper;

/**
 * Class Compiler
 * @package Snooper\Components\Compiler
 */
class Compiler implements ICompiler
{
    const LOG_COMPILER_KEY = 'Compiler';
    
    /**
     * @var array
     */
    protected $preCompileAdditionalTemplates = [];

    /**
     * @var array
     */
    protected $postCompileAdditionalTemplates = [];

    /**
     * @param $template
     */
    public function addPreCompileAdditionalTemplates($template)
    {
        $this->preCompileAdditionalTemplates[] = $template;
    }

    /**
     * @param $template
     */
    public function addPostCompileAdditionalTemplates($template)
    {
        $this->postCompileAdditionalTemplates[] = $template;
    }

    /**
     * @param $section
     * @throws \Exception
     */
    public function runSection($section)
    {
        LoggerContainer::instance()->add(self::LOG_COMPILER_KEY,sprintf('Starting compile "%s".',$section), ILog::BLOCK_BEGIN);

        $config = Snooper::instance()->getConfig();

        if ($section === $config->getFirstSection()) {
            $this->preCompile();
        }

        $this->compileSection($section);

        if ($section === $config->getLastSection()) {
            $this->postCompile();
        }

        LoggerContainer::instance()->add(self::LOG_COMPILER_KEY,sprintf('Finish compile "%s".',$section),ILog::BLOCK_END);
    }

    /**
     *
     */
    protected function preCompile()
    {
        LoggerContainer::instance()->add(self::LOG_COMPILER_KEY,sprintf('Starting compile section "first".'));

        $config = Snooper::instance()->getConfig();
        $renderer = Snooper::instance()->getRenderer();



        $template = $renderer->render('main.tpl',
            [
                '###DEBUG###'               => $config->isDebugMode() ? 'true':'false',
                '###VERSION###'             => Snooper::getVersion(),
                '###XHRSENDMODE###'         => $config->getXhrSendMode(),
                '###HEADERATTRIBUTENAME###' => $config->getJsonResponseKey()
            ]
        );

        CompiledContainer::instance()->add($config->getFirstSection(),$template);

        foreach ($this->preCompileAdditionalTemplates as $template) {
            CompiledContainer::instance()->add($config->getFirstSection(),$template);
        }

        LoggerContainer::instance()->add(self::LOG_COMPILER_KEY,sprintf('Finish compile section "first".'));
    }

    /**
     * @throws \Exception
     */
    protected function postCompile()
    {
        LoggerContainer::instance()->add(self::LOG_COMPILER_KEY,sprintf('Starting compile section "last".'));

        $config = Snooper::instance()->getConfig();

        foreach ($this->postCompileAdditionalTemplates as $template) {
            CompiledContainer::instance()->add($config->getLastSection(),$template);
        }

        $this->addRenderedOutput($config->getLastSection(),Deliver::LAST_SECTION);

        LoggerContainer::instance()->add(self::LOG_COMPILER_KEY,sprintf('Finish compile section "last".'));
    }

    /**
     * @param $section
     * @throws \Exception
     */
    protected function compileSection($section)
    {
        LoggerContainer::instance()->add(self::LOG_COMPILER_KEY,sprintf('Starting compile section (%s).',$section));

        $this->addRenderedOutput($section, Deliver::PRE_SECTION);
        $this->addProviderTemplates($section);
        $this->addRenderedOutput($section, Deliver::POST_SECTION);

        LoggerContainer::instance()->add(self::LOG_COMPILER_KEY,sprintf('Finish compile section (%s).',$section));
    }

    /**
     * @param $section
     * @param $deliverTimeOfExecution
     * @throws \Exception
     */
    protected function addRenderedOutput($section, $deliverTimeOfExecution)
    {
        $list = [];
        /** @var \Snooper\Components\Response\ICommonResponse $response */
        foreach (\Snooper\Components\Response\Container::instance()->getRaw() as &$response) {

            if (!$response->isDelivered()) {
                $deliver = $response->getDeliver();
                if ($deliver->getSection() === $section && $deliver->getDeliverTimeOfExecution() === $deliverTimeOfExecution) {
                    $list[] = $response->prepare();
                    $response->setDelivered(true);
                }
            }
        }

        if ($list) {
            $content = Snooper::instance()->getRenderer()->render('execution.tpl',['###JSON###'=>json_encode($list)]);
            CompiledContainer::instance()->add($section,$content);
        }
    }


    /**
     * @param $section
     * @todo: this step seems to be static and could be cached
     */
    protected function addProviderTemplates($section)
    {
        $compiledContainer = CompiledContainer::instance();

        /** @var IProvider $provider */
        foreach (ProviderContainer::instance()->getRaw() as $provider) {

            if ($provider->isActive()) {
                $callMethod = sprintf('get%sSection',ucfirst($section));
                if (method_exists($provider,$callMethod)) {
                    $content = $provider->$callMethod();
                    LoggerContainer::instance()->add(self::LOG_COMPILER_KEY,sprintf('Call "%s" on %s.',$callMethod, get_class($provider)));
                    if (strlen($content)) {
                        $compiledContainer->add($section,$content);
                    }
                } else {
                    LoggerContainer::instance()->add(self::LOG_COMPILER_KEY,sprintf('Method "%s" on %s not found.',$callMethod, get_class($provider)));
                }
            }
        }
    }
}
