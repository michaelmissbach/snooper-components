<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Exception;

/**
 * Class ProviderCallDefinitionException
 * @package Snooper\Components\Exception
 */
class ProviderCallDefinitionException extends \Exception
{
}