<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Processor;

use Snooper\Components\Exception\ProviderCallDefinitionException;
use Snooper\Components\Provider\IProvider;
use Snooper\Components\Provider\IProviderCallDefinition;
use Snooper\Components\Provider\ProviderCall;
use Snooper\Components\Provider\ProviderCallContainer;
use Snooper\Components\Provider\ProviderCallDefinition;
use Snooper\Components\Provider\ProviderContainer;
use Snooper\Components\EventTrigger\IEventTrigger;
use Snooper\Components\Log\ILog;
use Snooper\Components\Log\LoggerContainer;
use Snooper\Components\Response\Builder;
use Snooper\Components\Response\ICommonResponse;
use Snooper\Components\Response\Parameters\Deliver;
use Snooper\Components\Response\Parameters\IDeliver;
use Snooper\Components\Snooper;

/**
 * Class Processor
 * @package Snooper\Components\Processor
 */
class Processor implements IProcessor
{
    const LOG_PROCESSOR_KEY = 'Processor';

    /**
     * @param IEventTrigger $e
     * @throws \Exception
     */
    public function prepareProviderCallsByPrio(IEventTrigger $e)
    {
        LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,'Starting prepare provider calls.',ILog::BLOCK_BEGIN);

        $ProviderCallContainer = ProviderCallContainer::instance();
        $providerContainer = ProviderContainer::instance();
        $raisedEventName = $e->getEventName();

        $ProviderCallContainer->clear();

        foreach ($providerContainer->getYielded() as $provider) {
            if ($provider->isActive()) {
                $ProviderCallContainer->add(ProviderCall::create($provider, 'preRaiseEvent'),IProviderCallDefinition::PRIORIZATION_LOWEST - 1, true);
                LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,sprintf('%s: "preRaiseEvent"',get_class($provider)));
            }
        }
        foreach ($providerContainer->getYielded() as $provider) {
            if ($provider->isActive()) {
                $events = array_change_key_case($provider->getEvents(), CASE_LOWER);
                if (array_key_exists($raisedEventName, $events)) {
                    /** @var IProviderCallDefinition $providerCallDefinition */
                    $providerCallDefinition = $this->getExtractedAndFilteredProviderCallDefinition($events[$raisedEventName],$provider);

                    if (method_exists($provider,$providerCallDefinition->getMethod())) {
                        $ProviderCallContainer->add(
                            ProviderCall::create(
                                $provider,
                                $providerCallDefinition->getMethod(),
                                $providerCallDefinition->getAdditionalParameters()
                            ),
                            $providerCallDefinition->getPriorization()
                        );
                        LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,sprintf('%s: "%s"',get_class($provider),$providerCallDefinition->getMethod()));
                    }
                }
            }
        }
        foreach ($providerContainer->getYielded() as $provider) {
            if ($provider->isActive()) {
                $ProviderCallContainer->add(ProviderCall::create($provider, 'postRaiseEvent'),IProviderCallDefinition::PRIORIZATION_HIGHEST + 1, true);
                LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,sprintf('%s: "postRaiseEvent"',get_class($provider)));
            }
        }

        LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,'Finish prepare provider calls.',ILog::BLOCK_END);
    }

    /**
     * @param $settings
     * @param $provider
     * @return ProviderCallDefinition|null
     * @throws \Exception
     * @deprecated
     */
    protected function getExtractedAndFilteredProviderCallDefinition($settings,$provider)
    {
        $providerCallDefinition = null;

        switch (true) {
            case $settings instanceof IProviderCallDefinition:
                return $settings;
            case is_array($settings):
                if (!array_key_exists('method',$settings)) {
                    throw new ProviderCallDefinitionException('Method not specified. Missing settings key "method".');
                }
                $providerCallDefinition = ProviderCallDefinition::create($settings['method']);
                if (array_key_exists('additionalParameter',$settings)) {
                    $providerCallDefinition->setAdditionalParameters($settings['additionalParameter']);
                }
                if (array_key_exists('prio',$settings)) {
                    $providerCallDefinition->setPriorization((int)$settings['prio']);
                }

                LoggerContainer::instance()->add(
                    self::LOG_PROCESSOR_KEY,
                    sprintf('DEPRECATION: its deprecated to define event parameters as array defined in "%s". Use ProviderCallDefinition Object instead.',get_class($provider))
                );
                break;
            case is_string($settings):
                $providerCallDefinition = ProviderCallDefinition::create($settings);
                LoggerContainer::instance()->add(
                    self::LOG_PROCESSOR_KEY,
                    sprintf('DEPRECATION: its deprecated to define event method as string defined in "%s". Use ProviderCallDefinition Object instead.',get_class($provider))
                );
                break;
            default:
                throw new ProviderCallDefinitionException('Unsupported settings type. Use ProviderCallDefinition Object instead.');
        }

        return $providerCallDefinition;
    }

    /**
     * @param IEventTrigger $e
     * @throws \Exception
     */
    public function executesProviderCalls(IEventTrigger $e)
    {
        LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,'Starting execution events.',ILog::BLOCK_BEGIN);

        $providerCallContainer = ProviderCallContainer::instance();
        $raisedEventName = $e->getEventName();
        $config = Snooper::instance()->getConfig();

        $this->addLogResponse(Builder::createDeliver($config->getFirstSection(),Deliver::PRE_SECTION), sprintf('Event %s fired.',$raisedEventName));
        $this->addLogResponse(Builder::createDeliver($config->getFirstSection(),Deliver::PRE_SECTION), $e->getParameters()->all());

        $providerCallContainer->setSortModeAsc();
        $providerCallContainer->sortFlat();

        foreach ($providerCallContainer->getDeepYielded() as $singleExecution) {

            $provider               = $singleExecution->getProvider();
            $method                 = $singleExecution->getMethod();
            $additionalParameters   = $singleExecution->getAdditionalParameters();

            LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,sprintf('Calls %s: "%s"',get_class($provider),$method));

            try {

                switch (true) {
                    case $additionalParameters:
                        $response = $provider->$method($e,$additionalParameters);
                        break;
                    default:
                        $response = $provider->$method($e);
                        break;
                }

                $this->handleSendedResponses($provider,$response);

            } catch(\Exception $error) {

                $this->handleError($provider,$error);

            } catch(\Throwable $error) {

                $this->handleError($provider,$error);

            }

            if ($e->isPropagationStopped()) {
                LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,sprintf('Propagation stopped by %s.',get_class($provider)));
                break;
            }
        }

        LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,'Finish execution events.',ILog::BLOCK_END);
    }

    /**
     * @param $response
     */
    protected function handleSendedResponses($provider,$response)
    {
        switch(true) {
            case $response instanceof ICommonResponse:
                $response->setProvider($provider);
                \Snooper\Components\Response\Container::instance()->add($response);
                LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,$response->getSendResponseLog());
                break;
            case is_array($response):
                foreach ($response as $responseSingle) {
                    $this->handleSendedResponses($provider,$responseSingle);
                }
                break;
            default:
                break;
        }
    }

    /**
     * @param IProvider $provider
     * @param $error
     * @throws \Exception
     */
    protected function handleError(IProvider $provider, $error)
    {
        $config = Snooper::instance()->getConfig();

        LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,sprintf('Error by %s. (%s)',get_class($provider), $error->getMessage()));
        $this->addLogResponse(

            Builder::createDeliver($config->getFirstSection(),Deliver::PRE_SECTION),
            sprintf('Error on Provider %s: "%s". Error caused in File %s in Line %s.',get_class($provider), $error->getMessage(), $error->getFile(),$error->getLine()),
            true
        );
    }

    /**
     * @param IDeliver $deliver
     * @param $message
     * @param bool $isError
     * @throws \Exception
     */
    protected function addLogResponse(IDeliver $deliver, $message, $isError = false)
    {
        if (!Snooper::instance()->getConfig()->isDebugMode()) {
            return;
        }

        if (is_array($message) || is_object($message)) {

            LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,'Start serialize objects for debug.',ILog::BLOCK_BEGIN);
            $message = Snooper::instance()->getSerializer()->serialize($message);
            LoggerContainer::instance()->add(self::LOG_PROCESSOR_KEY,'Finished serialize objects for debug. (This may be a performance issue in debug mode. To resolve this reduce the nested_level in debug config.)',ILog::BLOCK_END);
        }

        \Snooper\Components\Response\Container::instance()->add( Builder::createStandardResponse(
                $deliver,
                'Snooper.log',
                Builder::createParameter($message,false,$isError,false)
            )
        );
    }
}
