<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Serialize;

use Snooper\Components\Snooper;

/**
 * Class Serializer
 * @package Snooper\Components\Serialize
 */
class Serializer implements ISerializer
{
    /**
     * @var int
     */
    protected $convertToArrayMaxNestedLevel = null;

    /**
     * @param $subject
     * @return []
     */
    public function serialize($subject)
    {
        $result = [];
        $this->convertToArrayMaxNestedLevel = Snooper::instance()->getConfig()->getDebugNestedLevel();

        $this->convertToArrayInternal($result,$subject);

        return json_encode($result);
    }

    /**
     * @param $arr
     * @param $something
     * @param array $hashList
     * @param int $currentLevel
     * @param int $maxLevel
     */
    protected function convertToArrayInternal(&$arr,$something,&$hashList = [],$currentLevel = 0)
    {
        if ($currentLevel > $this->convertToArrayMaxNestedLevel) {
            $arr = sprintf('Max recursion level (%s) reached.',$this->convertToArrayMaxNestedLevel);
            return;
        }

        //list instances it´s problematically to reflect:
        foreach (Snooper::instance()->getConfig()->getLogExcludedClasses() as $className) {
            try {
                if ($something instanceof $className) {
                    $arr = sprintf('%s not logged and skipped.',get_class($something));
                    return;
                }
            } catch (\Exception $error) {
                $arr = sprintf('%s not logged and skipped because of Errors (%s).',get_class($something), $error->getMessage());
                return;
            }
        }
        //endlist

        switch(true) {
            case is_array($something):
                foreach ($something as $key=>$item) {
                    if (is_null($arr)) {
                        $arr = [];
                    }
                    $this->convertToArrayInternal($arr[$key],$item,$hashList,$currentLevel+1);
                }
                break;
            case is_object($something):
                $hash = spl_object_hash($something);
                if (in_array($hash,$hashList)) {
                    return; //prevents circular reflection
                }
                $hashList[] = $hash;
                $reflection = new \ReflectionObject($something);
                $properties = $reflection->getStaticProperties();
                foreach ($properties as $name=>$value) {
                    if (is_null($arr)) {
                        $arr = [];
                    }
                    $this->convertToArrayInternal($arr[$name],$value,$hashList, $currentLevel+1);
                }
                $properties = $reflection->getProperties(
                    \ReflectionProperty::IS_PROTECTED |
                    \ReflectionProperty::IS_PRIVATE |
                    \ReflectionProperty::IS_PUBLIC);
                foreach ($properties as $property) {
                    if (is_null($arr)) {
                        $arr = [];
                    }
                    $property->setAccessible(true);
                    $this->convertToArrayInternal($arr[$property->getName()],$property->getValue($something),$hashList,$currentLevel+1);
                }
                break;
            default:
                $arr = $something;
                break;
        }
    }
}