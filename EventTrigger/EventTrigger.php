<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\EventTrigger;

use Snooper\Components\Utility\Bag;

/**
 * Class EventTrigger
 * @package Snooper\Components\EventTrigger
 */
class EventTrigger implements IEventTrigger
{
    /**
     * @var Bag
     */
    protected $parameters;

    /**
     * @var string
     */
    protected $eventName;

    /**
     * @var mixed
     */
    protected $context;

    /**
     * @var bool Whether no further event listeners should be triggered
     */
    protected $propagationStopped = false;

    /**
     * Event constructor.
     * @param $eventName
     * @param array $parameters
     */
    protected function __construct($eventName, $parameters = [])
    {
        $this->eventName = trim(strtolower($eventName));
        $this->parameters = new Bag();
        $this->undockParameters($parameters);
    }

    /**
     * @param $eventName
     * @param array $parameters
     * @return EventTrigger
     */
    public static function create($eventName, $parameters = [])
    {
        return new static($eventName,$parameters);
    }

    /**
     * @param array $parameters
     */
    protected function undockParameters(array $parameters)
    {
        foreach ($parameters as $key=>$parameter) {
            switch(true) {
                case is_object($parameter):
                    switch(true) {
                        case $parameter instanceof Bag:
                            $this->parameters->set($key, $parameter->all());
                            break;
                        default:
                            $this->parameters->set($key, clone $parameter);
                            break;
                    }
                    break;
                default:
                    $this->parameters->set($key,$parameter);
                    break;
            }
        }
    }

    /**
     * @return Bag
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }

    /**
     * @return bool
     */
    public function isPropagationStopped()
    {
        return $this->propagationStopped;
    }

    /**
     *
     */
    public function stopPropagation()
    {
        $this->propagationStopped = true;
    }

    /**
     * @return mixed
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param $context
     * @return EventTrigger
     */
    public function setContext($context)
    {
        $this->context = $context;
        return $this;
    }
}
