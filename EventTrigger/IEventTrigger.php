<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\EventTrigger;

use Snooper\Components\Context\IContext;
use Snooper\Components\Utility\Bag;

/**
 * Interface IEventTrigger
 * @package Snooper\Components\EventTrigger
 */
interface IEventTrigger
{
    /**
     * @param $eventName
     * @param array $parameters
     * @return mixed
     */
    public static function create($eventName, $parameters = []);

    /**
     * @return Bag
     */
    public function getParameters();

    /**
     * @return string
     */
    public function getEventName();

    /**
     * @return bool
     */
    public function isPropagationStopped();

    /**
     *
     */
    public function stopPropagation();

    /**
     * @return IContext
     */
    public function getContext();

    /**
     * @param $context
     * @return Event
     */
    public function setContext($context);
}
