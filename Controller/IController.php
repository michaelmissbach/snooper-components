<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Controller;

use Snooper\Components\EventTrigger\IEventTrigger;

/**
 * Interface IController
 * @package Snooper\Components\Controller
 */
interface IController
{
    /**
     * @param IEventTrigger $e
     * @throws \Exception
     */
    public function onRaiseEvent(IEventTrigger $e);

    /**
     * @param IEventTrigger $e
     * @throws \Exception
     */
    public function onRaiseEventExecution(IEventTrigger $e);

    /**
     * @param $section
     * @return string
     */
    public function outputSection($section);
}
