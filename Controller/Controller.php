<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Controller;

use Snooper\Components\Compiler\CompiledContainer;
use Snooper\Components\EventTrigger\IEventTrigger;
use Snooper\Components\Log\LoggerContainer;
use Snooper\Components\Snooper;

/**
 * Class Controller
 * @package Snooper\Components\Controller
 */
class Controller implements IController
{
    /**
     * @param IEventTrigger $e
     * @throws \Exception
     */
    public function onRaiseEvent(IEventTrigger $e)
    {
        LoggerContainer::instance()->setEnabled(Snooper::instance()->getConfig()->isDebugMode());

        $e->setContext(Snooper::instance()->getContext());

        Snooper::instance()->getProcessor()->prepareProviderCallsByPrio($e);
    }

    /**
     * @param IEventTrigger $e
     * @throws \Exception
     */
    public function onRaiseEventExecution(IEventTrigger $e)
    {
        LoggerContainer::instance()->setEnabled(Snooper::instance()->getConfig()->isDebugMode());

        Snooper::instance()->getProcessor()->executesProviderCalls($e);

        CompiledContainer::instance()->clear();
    }

    /**
     * @param $section
     * @return string
     * @throws \Exception
     */
    public function outputSection($section)
    {
        Snooper::instance()->getCompiler()->runSection($section);

        return implode('',CompiledContainer::instance()->getByGroup($section));
    }
}
