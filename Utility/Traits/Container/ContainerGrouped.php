<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Utility\Traits\Container;

/**
 * Trait ContainerGrouped
 * @package Snooper\Components\Utility\Traits
 */
trait ContainerGrouped
{
    /**
     * @param $section
     */
    protected function createGroup($group)
    {
        if (!is_array($this->container)) {
            $this->container = [];
        }
        if (!array_key_exists($group,$this->container)) {
            $this->container[$group] = [];
            $this->count++;
        }
    }

    /**
     * @param $group
     * @param $source
     */
    public function add($group,$source)
    {
        $this->createGroup($group);

        $this->container[$group][] = $source;
    }

    /**
     * @param $section
     * @return mixed
     */
    public function getByGroup($group)
    {
        return array_key_exists($group, $this->container) ? $this->container[$group] : [];
    }
}
