<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Utility\Traits\Container;

/**
 * Trait SortGroup
 * @package Snooper\Components\Traits
 */
trait SortGroup
{
    /**
     *
     */
    public function sortFlat()
    {
        switch (true) {
            case $this->sortMode === 1:
                ksort($this->container);
                break;
            case $this->sortMode === -1:
                krsort($this->container);
                break;
            default:
                break;
        }
    }

    /**
     *
     */
    public function sortDeep()
    {
        foreach ($this->container as &$values) {

            switch (true) {
                case $this->sortMode === 1:
                    asort($values);
                    break;
                case $this->sortMode === -1:
                    arsort($values);
                    break;
                default:
                    break;
            }

        }

    }
}
