<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Utility\Traits\Container;

/**
 * Trait SortFlat
 * @package Snooper\Components\Traits
 */
trait SortFlat
{
    /**
     *
     */
    public function sort()
    {
        switch (true) {
            case $this->sortMode === 1:
                asort($this->container);
                break;
            case $this->sortMode === -1:
                arsort($this->container);
                break;
            default:
                break;
        }
    }
}
