<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Utility\Traits\Container;

/**
 * Trait Sortable
 * @package Snooper\Components\Traits
 */
trait Sortable
{
    /**
     * @var int
     */
    protected $sortMode = 1;

    /**
     *
     */
    public function setSortModeAsc()
    {
        $this->sortMode = 1;
    }

    /**
     *
     */
    public function setSortModeDesc()
    {
        $this->sortMode = -1;
    }
}
