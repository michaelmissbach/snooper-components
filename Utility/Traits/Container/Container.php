<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Utility\Traits\Container;

/**
 * Trait Container
 * @package Snooper\Components\Utility\Traits
 */
trait Container
{
    /**
     * @var []
     */
    protected $container = [];

    /**
     * @var int
     */
    protected $count = 0;

    /**
     *
     */
    public function clear()
    {
        $this->container = [];
        $this->count = 0;
    }

    /**
     * @return array
     */
    public function getRaw()
    {
        return $this->container;
    }

    /**
     * @return array|\Generator
     */
    public function getYielded()
    {
        foreach ($this->container as $entry) {
            yield $entry;
        }
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->count;
    }
}
