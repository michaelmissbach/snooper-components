<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Utility\Traits;

/**
 * Trait ConstantFinderTrait
 * @package Snooper\Components\Utility\Traits
 */
trait ConstantFinderTrait
{
    /**
     * @param $search
     * @return bool
     * @throws \ReflectionException
     */
    protected function isValueInConstants($search)
    {
        if (!$this->__constants) {
            $this->__constants = (new \ReflectionClass($this))->getConstants();
        }

        foreach ($this->__constants as $value) {
            if ($search === $value) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $search
     * @return bool
     * @throws \ReflectionException
     */
    protected function getConstantNames($maxCount = null)
    {
        if (!$this->__constants) {
            $this->__constants = (new \ReflectionClass($this))->getConstants();
        }

        $counter = 0;
        $constants = '';
        foreach ($this->__constants as $name=>$value) {
            if ($maxCount && $maxCount > $counter) {
                if (strlen($constants)) {
                    $constants.=',...';
                }
                break;
            }
            if (strlen($constants)) {
                $constants.=',';
            }
            $constants.=sprintf('"%s"',$name);
            $counter++;
        }
        return $constants;
    }
}
