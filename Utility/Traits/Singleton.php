<?php

/*
 * This file is part of the Snooper component package.
 *
 * (c) Michael Missbach <michael@missbach-world.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Snooper\Components\Utility\Traits;

/**
 * Trait Singleton
 * @package Snooper\Components\Utility\Traits
 */
trait Singleton
{
    /**
     * @var mixed
     */
    protected static $_instance = null;

    /**
     * @return $this
     */
    public static function instance()
    {
        if (null === static::$_instance)
        {
            static::$_instance = new static;
        }
        return static::$_instance;
    }

    /**
     *
     */
    protected function __clone() {}

    /**
     * ContainerBase constructor.
     */
    protected function __construct() {}
}
